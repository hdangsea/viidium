<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_viidium
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$params = JComponentHelper::getParams('com_viidium');

define('COM_VIIDIUM_BASE',    JPATH_ROOT . '/listings');

$controller	= JControllerLegacy::getInstance('Viidium', array('base_path' => JPATH_COMPONENT_ADMINISTRATOR));
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();