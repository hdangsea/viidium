<?php
defined('_JEXEC') or die;

class ViidiumViewViidium extends JViewLegacy {
    public function display($tpl = null)
    {
        $app = JFactory::getApplication();
        $config = JComponentHelper::getParams('com_viidium');

        if (!$app->isAdmin()) {
            return $app->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR'), 'warning');
        }

        $lang = JFactory::getLanguage();

        $document = JFactory::getDocument();



        if (DIRECTORY_SEPARATOR == '\\') {
            $base = str_replace(DIRECTORY_SEPARATOR, "\\\\", COM_VIIDIUM_BASE);
        } else {
            $base = COM_VIIDIUM_BASE;
        }

        parent::display($tpl);
    }
}
