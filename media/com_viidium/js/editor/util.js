if (typeof Viidium === 'undefined') {
    Viidium = {};
}

Viidium.createReplacements = function() {
    return [

        // replace two bogus tags that begin pastes from google docs
        [new RegExp(/<[^>]*docs-internal-guid[^>]*>/gi), ""],
        [new RegExp(/<\/b>(<br[^>]*>)?$/gi), ""],

        // un-html spaces and newlines inserted by OS X
        [new RegExp(/<span class="Apple-converted-space">\s+<\/span>/g), ' '],
        [new RegExp(/<br class="Apple-interchange-newline">/g), '<br>'],

        // replace google docs italics+bold with a span to be replaced once the html is inserted
        [new RegExp(/<span[^>]*(font-style:italic;font-weight:bold|font-weight:bold;font-style:italic)[^>]*>/gi), '<span class="replace-with italic bold">'],

        // replace google docs italics with a span to be replaced once the html is inserted
        [new RegExp(/<span[^>]*font-style:italic[^>]*>/gi), '<span class="replace-with italic">'],

        //[replace google docs bolds with a span to be replaced once the html is inserted
        [new RegExp(/<span[^>]*font-weight:bold[^>]*>/gi), '<span class="replace-with bold">'],

        // replace manually entered b/i/a tags with real ones
        [new RegExp(/&lt;(\/?)(i|b|a)&gt;/gi), '<$1$2>'],

        // replace manually a tags with real ones, converting smart-quotes from google docs
        [new RegExp(/&lt;a\s+href=(&quot;|&rdquo;|&ldquo;|“|”)([^&]+)(&quot;|&rdquo;|&ldquo;|“|”)&gt;/gi), '<a href="$2">']

    ];
}

Viidium.Util = {
    // http://stackoverflow.com/questions/17907445/how-to-detect-ie11#comment30165888_17907562
    // by rg89
    isIE: ((navigator.appName === 'Microsoft Internet Explorer') || ((navigator.appName === 'Netscape') && (new RegExp('Trident/.*rv:([0-9]{1,}[.0-9]{0,})').exec(navigator.userAgent) !== null))),

    // https://github.com/jashkenas/underscore
    keyCode: {
        BACKSPACE: 8,
        TAB: 9,
        ENTER: 13,
        ESCAPE: 27,
        SPACE: 32,
        DELETE: 46
    },

    parentElements: ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'pre'],

    defaults: function defaults(dest, source) {
        return copyInto(dest, source);
    },

    extend: function extend(dest, source) {
        return copyInto(dest, source, true);
    },

    derives: function derives(base, derived) {
        var origPrototype = derived.prototype;
        function Proto() { }
        Proto.prototype = base.prototype;
        derived.prototype = new Proto();
        derived.prototype.constructor = base;
        derived.prototype = copyInto(derived.prototype, origPrototype);
        return derived;
    },

    // Find the next node in the DOM tree that represents any text that is being
    // displayed directly next to the targetNode (passed as an argument)
    // Text that appears directly next to the current node can be:
    //  - A sibling text node
    //  - A descendant of a sibling element
    //  - A sibling text node of an ancestor
    //  - A descendant of a sibling element of an ancestor
    findAdjacentTextNodeWithContent: function findAdjacentTextNodeWithContent(rootNode, targetNode, ownerDocument) {
        var pastTarget = false,
            nextNode,
            nodeIterator = ownerDocument.createNodeIterator(rootNode, NodeFilter.SHOW_TEXT, null, false);

        // Use a native NodeIterator to iterate over all the text nodes that are descendants
        // of the rootNode.  Once past the targetNode, choose the first non-empty text node
        nextNode = nodeIterator.nextNode();
        while (nextNode) {
            if (nextNode === targetNode) {
                pastTarget = true;
            } else if (pastTarget) {
                if (nextNode.nodeType === 3 && nextNode.nodeValue && nextNode.nodeValue.trim().length > 0) {
                    break;
                }
            }
            nextNode = nodeIterator.nextNode();
        }

        return nextNode;
    },

    isDescendant: function isDescendant(parent, child) {
        if (!parent || !child) {
            return false;
        }
        var node = child.parentNode;
        while (node !== null) {
            if (node === parent) {
                return true;
            }
            node = node.parentNode;
        }
        return false;
    },

    // https://github.com/jashkenas/underscore
    isElement: function isElement(obj) {
        return !!(obj && obj.nodeType === 1);
    },

    now: function now() {
        return Date.now || new Date().getTime();
    },

    // https://github.com/jashkenas/underscore
    throttle: function throttle(func, wait) {
        var THROTTLE_INTERVAL = 50,
            context,
            args,
            result,
            timeout = null,
            previous = 0,
            later;

        if (!wait && wait !== 0) {
            wait = THROTTLE_INTERVAL;
        }

        later = function () {
            previous = Viidium.Util.now();
            timeout = null;
            result = func.apply(context, args);
            if (!timeout) {
                context = args = null;
            }
        };

        return function () {
            var currNow = Viidium.Util.now(),
                remaining = wait - (currNow - previous);
            context = this;
            args = arguments;
            if (remaining <= 0 || remaining > wait) {
                clearTimeout(timeout);
                timeout = null;
                previous = currNow;
                result = func.apply(context, args);
                if (!timeout) {
                    context = args = null;
                }
            } else if (!timeout) {
                timeout = setTimeout(later, remaining);
            }
            return result;
        };
    },

    traverseUp: function (current, testElementFunction) {

        do {
            if (current.nodeType === 1) {
                if (testElementFunction(current)) {
                    return current;
                }
                // do not traverse upwards past the nearest containing editor
                if (current.getAttribute('data-medium-element')) {
                    return false;
                }
            }

            current = current.parentNode;
        } while (current);

        return false;

    },

    htmlEntities: function (str) {
        // converts special characters (like <) into their escaped/encoded values (like &lt;).
        // This allows you to show to display the string without the browser reading it as HTML.
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    },

    // http://stackoverflow.com/questions/6690752/insert-html-at-caret-in-a-contenteditable-div
    insertHTMLCommand: function (doc, html) {
        var selection, range, el, fragment, node, lastNode;

        selection = window.getSelection();

        if (selection.focusNode.parentElement.getAttribute('data-placeholder')) {
            selection.focusNode.parentElement.outerHTML = html;
            return;
        }

        if (doc.queryCommandSupported('insertHTML')) {
            try {
                return doc.execCommand('insertHTML', false, html);
            } catch (ignore) {}
        }


        if (selection.getRangeAt && selection.rangeCount) {
            range = selection.getRangeAt(0);
            range.deleteContents();

            el = doc.createElement("div");
            el.innerHTML = html;
            fragment = doc.createDocumentFragment();
            while (el.firstChild) {
                node = el.firstChild;
                lastNode = fragment.appendChild(node);
            }
            range.insertNode(fragment);

            // Preserve the selection:
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                range.collapse(true);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }
    },

    // TODO: not sure if this should be here
    setTargetBlank: function (el) {
        var i;
        if (el.tagName.toLowerCase() === 'a') {
            el.target = '_blank';
        } else {
            el = el.getElementsByTagName('a');

            for (i = 0; i < el.length; i += 1) {
                el[i].target = '_blank';
            }
        }
    },

    isListItemChild: function (node) {
        var parentNode = node.parentNode,
            tagName = parentNode.tagName.toLowerCase();
        while (this.parentElements.indexOf(tagName) === -1 && tagName !== 'div') {
            if (tagName === 'li') {
                return true;
            }
            parentNode = parentNode.parentNode;
            if (parentNode && parentNode.tagName) {
                tagName = parentNode.tagName.toLowerCase();
            } else {
                return false;
            }
        }
        return false;
    }
};

Viidium.Selection = {
    // http://stackoverflow.com/questions/1197401/how-can-i-get-the-element-the-caret-is-in-with-javascript-when-using-contentedi
    // by You
    getSelectionStart: function (ownerDocument) {
        var node = ownerDocument.getSelection().anchorNode,
            startNode = (node && node.nodeType === 3 ? node.parentNode : node);
        return startNode;
    },

    findMatchingSelectionParent: function (testElementFunction, contentWindow) {
        var selection = contentWindow.getSelection();

        if (selection.rangeCount === 0) {
            return false;
        }

        var range = selection.getRangeAt(0);
        var startNode = range.startContainer.parentNode;
        var endNode = range.endContainer.parentNode;

        if (startNode.getAttribute('data-title') || startNode.getAttribute('data-introduction')
            || endNode.getAttribute('data-title') || endNode.getAttribute('data-introduction')) {
            return false;
        }

        var current = range.commonAncestorContainer;

        current = range.commonAncestorContainer;

        return Viidium.Util.traverseUp(current, testElementFunction);
    },

    getSelectionElement: function (contentWindow) {
        return this.findMatchingSelectionParent(function (el) {
            return el.getAttribute('data-medium-element');
        }, contentWindow);
    },

    selectionInContentEditableFalse: function (contentWindow) {
        return this.findMatchingSelectionParent(function (el) {
            return (el && el.nodeName !== '#text' && el.getAttribute('contenteditable') === 'false');
        }, contentWindow);
    },

    // http://stackoverflow.com/questions/4176923/html-of-selected-text
    // by Tim Down
    getSelectionHtml: function getSelectionHtml() {
        var i,
            html = '',
            sel,
            len,
            container;
        if (this.options.contentWindow.getSelection !== undefined) {
            sel = this.options.contentWindow.getSelection();
            if (sel.rangeCount) {
                container = this.options.ownerDocument.createElement('div');
                for (i = 0, len = sel.rangeCount; i < len; i += 1) {
                    container.appendChild(sel.getRangeAt(i).cloneContents());
                }
                html = container.innerHTML;
            }
        } else if (this.options.ownerDocument.selection !== undefined) {
            if (this.options.ownerDocument.selection.type === 'Text') {
                html = this.options.ownerDocument.selection.createRange().htmlText;
            }
        }
        return html;
    },

    /**
     *  Find the caret position within an element irrespective of any inline tags it may contain.
     *
     *  @param {DOMElement} An element containing the cursor to find offsets relative to.
     *  @param {Range} A Range representing cursor position. Will window.getSelection if none is passed.
     *  @return {Object} 'left' and 'right' attributes contain offsets from begining and end of Element
     */
    getCaretOffsets: function getCaretOffsets(element, range) {
        var preCaretRange, postCaretRange;

        if (!range) {
            range = window.getSelection().getRangeAt(0);
        }

        preCaretRange = range.cloneRange();
        postCaretRange = range.cloneRange();

        preCaretRange.selectNodeContents(element);
        preCaretRange.setEnd(range.endContainer, range.endOffset);

        postCaretRange.selectNodeContents(element);
        postCaretRange.setStart(range.endContainer, range.endOffset);

        return {
            left: preCaretRange.toString().length,
            right: postCaretRange.toString().length
        };
    },

    // http://stackoverflow.com/questions/15867542/range-object-get-selection-parent-node-chrome-vs-firefox
    rangeSelectsSingleNode: function (range) {
        var startNode = range.startContainer;
        return startNode === range.endContainer &&
        startNode.hasChildNodes() &&
        range.endOffset === range.startOffset + 1;
    },

    getSelectedParentElement: function (range) {
        var selectedParentElement = null;
        if (this.rangeSelectsSingleNode(range) && range.startContainer.childNodes[range.startOffset].nodeType !== 3) {
            selectedParentElement = range.startContainer.childNodes[range.startOffset];
        } else if (range.startContainer.nodeType === 3) {
            selectedParentElement = range.startContainer.parentNode;
        } else {
            selectedParentElement = range.startContainer;
        }
        return selectedParentElement;
    },

    getSelectionData: function (el) {
        var tagName;

        if (el && el.tagName) {
            tagName = el.tagName.toLowerCase();
        }

        while (el && Viidium.Util.parentElements.indexOf(tagName) === -1) {
            el = el.parentNode;
            if (el && el.tagName) {
                tagName = el.tagName.toLowerCase();
            }
        }

        return {
            el: el,
            tagName: tagName
        };
    }
};

Viidium.pasteHandler = {
    handlePaste: function (element, evt, options) {
        var paragraphs,
            html = '',
            p,
            dataFormatHTML = 'text/html',
            dataFormatPlain = 'text/plain';

        element.classList.remove('medium-editor-placeholder');
        if (!options.forcePlainText && !options.cleanPastedHTML) {
            return element;
        }

        if (options.contentWindow.clipboardData && evt.clipboardData === undefined) {
            evt.clipboardData = options.contentWindow.clipboardData;
            // If window.clipboardData exists, but e.clipboardData doesn't exist,
            // we're probably in IE. IE only has two possibilities for clipboard
            // data format: 'Text' and 'URL'.
            //
            // Of the two, we want 'Text':
            dataFormatHTML = 'Text';
            dataFormatPlain = 'Text';
        }

        if (evt.clipboardData && evt.clipboardData.getData && !evt.defaultPrevented) {
            evt.preventDefault();

            var currentNode = window.getSelection().focusNode.parentElement;
            while (currentNode && (!currentNode.getAttribute('data-title')
                && !currentNode.getAttribute('data-introduction')
                && !currentNode.getAttribute('data-content'))) {
                currentNode = currentNode.parentElement;
            }

            if (options.cleanPastedHTML && evt.clipboardData.getData(dataFormatHTML)) {
                return this.cleanPaste(evt.clipboardData.getData(dataFormatHTML), options);
            }
            if (!(options.disableReturn || element.getAttribute('data-disable-return'))) {
                paragraphs = evt.clipboardData.getData(dataFormatPlain).split(/[\r\n]/g);

                if (currentNode.getAttribute('data-title') || currentNode.getAttribute('data-introduction')) {
                    for (p = 0; p < paragraphs.length; p += 1) {
                        if (paragraphs[p] !== '') {
                            html += Viidium.Util.htmlEntities(paragraphs[p]);
                        }
                    }
                } else {
                    for (p = 0; p < paragraphs.length; p += 1) {
                        if (paragraphs[p] !== '') {
                            html += '<p>' + Viidium.Util.htmlEntities(paragraphs[p]) + '</p>';
                        }
                    }
                }
                Viidium.Util.insertHTMLCommand(options.ownerDocument, html);
            } else {
                html = Viidium.Util.htmlEntities(evt.clipboardData.getData(dataFormatPlain));
                Viidium.Util.insertHTMLCommand(options.ownerDocument, html);
            }
        }
    },

    cleanPaste: function (text, options) {
        var i, elList, workEl,
            el = Viidium.Selection.getSelectionElement(options.contentWindow),
            multiline = /<p|<br|<div/.test(text),
            replacements = Viidium.createReplacements();

        for (i = 0; i < replacements.length; i += 1) {
            text = text.replace(replacements[i][0], replacements[i][1]);
        }

        if (multiline) {
            // double br's aren't converted to p tags, but we want paragraphs.
            elList = text.split('<br><br>');

            this.pasteHTML('<p>' + elList.join('</p><p>') + '</p>', options.ownerDocument);

            try {
                options.ownerDocument.execCommand('insertText', false, "\n");
            } catch (ignore) { }

            // block element cleanup
            elList = el.querySelectorAll('a,p,div,br');
            for (i = 0; i < elList.length; i += 1) {
                workEl = elList[i];

                switch (workEl.tagName.toLowerCase()) {
                    case 'a':
                        if (options.targetBlank) {
                            Viidium.Util.setTargetBlank(workEl);
                        }
                        break;
                    case 'p':
                    case 'div':
                        this.filterCommonBlocks(workEl);
                        break;
                    case 'br':
                        this.filterLineBreak(workEl);
                        break;
                }
            }
        } else {
            this.pasteHTML(text, options.ownerDocument);
        }
    },

    pasteHTML: function (html, ownerDocument) {
        var elList, workEl, i, fragmentBody, pasteBlock = ownerDocument.createDocumentFragment();

        pasteBlock.appendChild(ownerDocument.createElement('body'));

        fragmentBody = pasteBlock.querySelector('body');
        fragmentBody.innerHTML = html;

        this.cleanupSpans(fragmentBody, ownerDocument);

        elList = fragmentBody.querySelectorAll('*');
        for (i = 0; i < elList.length; i += 1) {
            workEl = elList[i];

            // delete ugly attributes
            workEl.removeAttribute('class');
            workEl.removeAttribute('style');
            workEl.removeAttribute('dir');

            if (workEl.tagName.toLowerCase() === 'meta') {
                workEl.parentNode.removeChild(workEl);
            }
        }
        Viidium.Util.insertHTMLCommand(ownerDocument, fragmentBody.innerHTML.replace(/&nbsp;/g, ' '));
    },

    isCommonBlock: function (el) {
        return (el && (el.tagName.toLowerCase() === 'p' || el.tagName.toLowerCase() === 'div'));
    },

    filterCommonBlocks: function (el) {
        if (/^\s*$/.test(el.textContent)) {
            el.parentNode.removeChild(el);
        }
    },

    filterLineBreak: function (el) {
        if (this.isCommonBlock(el.previousElementSibling)) {
            // remove stray br's following common block elements
            this.removeWithParent(el);
        } else if (this.isCommonBlock(el.parentNode) && (el.parentNode.firstChild === el || el.parentNode.lastChild === el)) {
            // remove br's just inside open or close tags of a div/p
            this.removeWithParent(el);
        } else if (el.parentNode.childElementCount === 1 && el.parentNode.textContent === '') {
            // and br's that are the only child of elements other than div/p
            this.removeWithParent(el);
        }
    },

    // remove an element, including its parent, if it is the only element within its parent
    removeWithParent: function (el) {
        if (el && el.parentNode) {
            if (el.parentNode.parentNode && el.parentNode.childElementCount === 1) {
                el.parentNode.parentNode.removeChild(el.parentNode);
            } else {
                el.parentNode.removeChild(el);
            }
        }
    },

    cleanupSpans: function (container_el, ownerDocument) {
        var i,
            el,
            new_el,
            spans = container_el.querySelectorAll('.replace-with'),
            isCEF = function (el) {
                return (el && el.nodeName !== '#text' && el.getAttribute('contenteditable') === 'false');
            };

        for (i = 0; i < spans.length; i += 1) {
            el = spans[i];
            new_el = ownerDocument.createElement(el.classList.contains('bold') ? 'b' : 'i');

            if (el.classList.contains('bold') && el.classList.contains('italic')) {
                // add an i tag as well if this has both italics and bold
                new_el.innerHTML = '<i>' + el.innerHTML + '</i>';
            } else {
                new_el.innerHTML = el.innerHTML;
            }
            el.parentNode.replaceChild(new_el, el);
        }

        spans = container_el.querySelectorAll('span');
        for (i = 0; i < spans.length; i += 1) {
            el = spans[i];

            // bail if span is in contenteditable = false
            if (Viidium.Util.traverseUp(el, isCEF)) {
                return false;
            }

            // remove empty spans, replace others with their contents
            if (/^\s*$/.test()) {
                el.parentNode.removeChild(el);
            } else {
                el.parentNode.replaceChild(ownerDocument.createTextNode(el.textContent), el);
            }
        }
    }
}