;(function ($, window, document, undefined) {

    'use strict';

    /** Default values */
    var pluginName = 'mediumInsert',
        addonName = 'Coverimage', // first char is uppercase
        defaults = {
            label: '<span class="fa fa-camera"></span>'
        };

    /**
     * Images object
     *
     * Sets options, variables and calls init() function
     *
     * @constructor
     * @param {DOM} el - DOM element to init the plugin on
     * @param {object} options - Options to override defaults
     * @return {void}
     */

    function Coverimage (el, options) {
        this.el = el;
        this.$el = $(el);
        this.templates = window.MediumInsert.Templates;

        this.options = $.extend(true, {}, defaults, options);

        this._defaults = defaults;
        this._name = pluginName;

        // Allow image preview only in browsers, that support's that
        if (this.options.preview && !window.FileReader) {
            this.options.preview = false;
        }

        this.init();
    }

    /**
     * Initialization
     *
     * @return {void}
     */

    Coverimage.prototype.init = function() {
        this.initExistingPhoto();
        this.addButton();
        this.events();
    };

    Coverimage.prototype.initExistingPhoto = function() {
        if (this.options.image !== '') {
            if ($('#vi-image-cover-bg').length == 0) {
                $('<img id="vi-image-cover-bg" />').insertBefore($(this.$el));
            }
            $('#vi-image-cover-bg').attr('src', this.options.image);
        }
    };

    Coverimage.prototype.addButton = function() {
        $(this.$el).parent().append(
            '<span class="vi-image-icon medium-insert-coverimage">' +
                '<i class="vi-image-picture uk-icon-picture-o"></i>' +
                '<i class="vi-image-delete uk-icon-close"></i>' +
            '</span>' +
            '<input type="file" id="vi-upload-cover-image" name="cover_photo" />' +
            '<input type="hidden" id="vi-remove-cover-image" name="remove_image" value="0" />'
        );

        if ($('#vi-image-cover-bg').length == 0 || $('#vi-image-cover-bg').attr('src') == '') {
            $(this.$el).nextAll('.medium-insert-coverimage').find('.vi-image-delete').fadeOut();
        }
    };

    /**
     * Event listeners
     *
     * @return {void}
     */
    Coverimage.prototype.events = function () {
        $(this.$el).nextAll('.medium-insert-coverimage').find('.vi-image-picture').on('click', $.proxy(this, 'insertCoverImage'));
        $(this.$el).nextAll('.medium-insert-coverimage').find('.vi-image-delete').on('click', $.proxy(this, 'deleteCoverImage'));

        var me = this;
        $('#vi-upload-cover-image').on('change', function(event) {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    if ($('#vi-image-cover-bg').length == 0) {
                        $('<img id="vi-image-cover-bg" />').insertBefore($(me.$el));
                    }
                    $('#vi-image-cover-bg').attr('src', event.target.result);

                    $(me.$el).nextAll('.medium-insert-coverimage').find('.vi-image-delete').fadeIn();
                }

                reader.readAsDataURL(this.files[0]);
                $('#vi-remove-cover-image').val(0);
            }
        });
    };

    Coverimage.prototype.deleteCoverImage = function(input) {
        $('#vi-image-cover-bg').remove();
        $(this.$el).nextAll('.medium-insert-coverimage').find('.vi-image-delete').fadeOut();
        $('#vi-upload-cover-image').val('');
        $('#vi-remove-cover-image').val(1);
    };

    Coverimage.prototype.insertCoverImage = function() {
        $('#vi-upload-cover-image').trigger('click');
    };

    /**
     * Get the Core object
     *
     * @return {object} Core object
     */
    Coverimage.prototype.getCore = function () {
        if (typeof(this.core) === 'undefined') {
            this.core = this.$el.data('plugin_'+ pluginName);
        }

        return this.core;
    };

    /**
     * Add uploaded / preview image to DOM
     *
     * @param {string} img
     * @returns {void}
     */

    Coverimage.prototype.showImage = function (img, data) {
        var $place, domImage;

        // If preview is allowed and preview image already exists,
        // replace it with uploaded image
        if (this.options.preview && data.context) {
            domImage = this.getDOMImage();
            domImage.onload = function () {
                data.context.find('img').attr('src', domImage.src);
            };
            domImage.src = img;
        } else {
            $place = this.$el.find('.medium-insert-active');

            data.context = $(this.templates['src/js/templates/extimages-image.hbs']({
                img: img,
                progress: this.options.preview
            })).appendTo($place);

            $place.find('br').remove();

            if (this.options.preview) {
                data.submit();
            }
        }
    };

    Coverimage.prototype.getDOMImage = function () {
        return new window.Image();
    };

    /**
     * Remove image
     *
     * @param {Event} e
     * @returns {void}
     */

    Coverimage.prototype.removeImage = function (e) {
        var $image, $parent, $empty;

        if (e.which === 8 || e.which === 46) {
            $image = this.$el.find('.medium-insert-image-active');

            if ($image.length) {
                e.preventDefault();

                this.deleteFile($image.attr('src'));

                $parent = $image.closest('.medium-insert-images');
                $image.closest('figure').remove();

                $('.medium-insert-images-toolbar').remove();

                if ($parent.find('figure').length === 0) {
                    $empty = $(this.templates['src/js/templates/core-empty-line.hbs']().trim());
                    $parent.before($empty);
                    $parent.remove();

                    // Hide addons
                    this.getCore().hideAddons();

                    this.getCore().moveCaret($empty);
                }

                this.$el.trigger('input');
            }
        }
    };

    /** Plugin initialization */

    $.fn[pluginName + addonName] = function (options) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName + addonName)) {
                $.data(this, 'plugin_' + pluginName + addonName, new Coverimage(this, options));
            }
        });
    };

})(jQuery, window, document);
