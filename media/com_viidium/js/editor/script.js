jQuery(function($) {
    function extend(b, a) {
        var prop;
        if (b === undefined) {
            return a;
        }
        for (prop in a) {
            if (a.hasOwnProperty(prop) && b.hasOwnProperty(prop) === false) {
                b[prop] = a[prop];
            }
        }
        return b;
    }

    ViidiumEditor = function() {}

    ViidiumEditor.prototype = new MediumEditor;
    ViidiumEditor.prototype.defaults = extend(MediumEditor.prototype.defaults, {
        postContent: '',
        postIntroduction: '',
        postTitle: ''
    });
    ViidiumEditor.constructor = ViidiumEditor;

    ViidiumEditor.prototype.init = function(elements, options) {
        MediumEditor.prototype.init.call(this, elements, options);

        // content block
        if (options.postContent == '') {
            this.contentPlaceholder = $('<span/>', {
                text: 'Type your content here',
                class: 'placeholder-content',
                'data-placeholder': 1
            });
            this.contentText = $('<div/>', {
                id: 'vi-post-content',
                class: 'content',
                'data-content': 1
                //'data-medium-element': 1
            });
            this.contentText.append('<p></p>');
            this.contentText.children().append(this.contentPlaceholder);
        } else {
            this.contentText = $(options.postContent);
        }
        $(this.elements).prepend(this.contentText);

        // introduction block
        if (options.postIntroduction == '') {
            this.introductionTextPlaceholder = $('<span/>', {
                text: 'Please type the introduction text',
                class: 'placeholder-introduction-text',
                'data-placeholder': 1
            });
            this.introductionText = $('<' + this.options.secondHeader + '/>', {
                id: 'vi-post-introduction',
                'data-introduction': 1,
                'style': 'text-align:center',
                'class': 'vi-introduction-text',
                'data-disable-toolbar': 1
            });
            this.introductionText.append(this.introductionTextPlaceholder);
        } else {
            this.introductionText = $(options.postIntroduction);
        }
        $(this.elements).prepend(this.introductionText);

        // title block
        if (options.postTitle == '') {
            this.titlePlaceholder = $('<span/>', {
                text: 'Title',
                class: 'placeholder-title',
                'data-placeholder': 1
            });
            this.title = $('<' + this.options.firstHeader + '/>', {
                'data-title': 1,
                'style': 'text-align:center',
                'id': 'vi-post-title',
                'data-disable-toolbar': 1
            });
            this.title.append(this.titlePlaceholder);
        } else {
            this.title = $(options.postTitle)
        }
        $(this.elements).prepend(this.title);

        this.elements[0].focus();
    };

    ViidiumEditor.prototype.bindParagraphCreation = function(index) {
        var self = this;
        this.on(this.elements[index], 'keypress', function (e) {
            var node = node = Viidium.Selection.getSelectionStart(self.options.ownerDocument);
            var tagName;

            if (e.which === Viidium.Util.keyCode.SPACE) {
                tagName = node.tagName.toLowerCase();
                if (tagName === 'a') {
                    self.options.ownerDocument.execCommand('unlink', false, null);
                }
            }

            if (node.getAttribute('data-placeholder') == 1) {
                if (node.parentElement.getAttribute('data-content')) {
                    node.innerHTML = '';
                    node.tagName = 'p';
                } else {
                    node.outerHTML = '';
                }
            } else if (node.getAttribute('data-title') == 1 && node.childElementCount > 0) {
                node.innerHTML = '';
            } else if (node.getAttribute('data-introduction') == 1 && node.childElementCount > 0) {
                node.innerHTML = '';
            } else if (node.getAttribute('data-content') == 1 && node.childElementCount > 0) {
                node.innerHTML = '<p></p>';
            }
        });

        this.on(this.elements[index], 'keyup', function (e) {
            var node = Viidium.Selection.getSelectionStart(self.options.ownerDocument),
                tagName,
                editorElement;

            if (node && node.getAttribute('data-medium-element') && node.children.length === 0 && !(self.options.disableReturn || node.getAttribute('data-disable-return'))) {
                self.options.ownerDocument.execCommand('formatBlock', false, 'p');
            }

            if (e.which === Viidium.Util.keyCode.ENTER) {
                node = Viidium.Selection.getSelectionStart(self.options.ownerDocument);
                tagName = node.tagName.toLowerCase();
                editorElement = Viidium.Selection.getSelectionElement(self.options.contentWindow);

                if (!(self.options.disableReturn || editorElement.getAttribute('data-disable-return')) &&
                    tagName !== 'li' && !Viidium.Util.isListItemChild(node)) {
                    if (!e.shiftKey) {

                        // paragraph creation should not be forced within a header tag
                        if (!/h\d/.test(tagName)) {
                            self.options.ownerDocument.execCommand('formatBlock', false, 'p');
                        }
                    }
                    if (tagName === 'a') {
                        self.options.ownerDocument.execCommand('unlink', false, null);
                    }
                }
            }

            var tagName = String.toLowerCase(node.tagName);
            if (tagName == 'span' || tagName == 'p') {
                node = node.parentNode;
            }
            if (node.getAttribute('data-title') == 1 && node.innerText.trim() == '') {
                node.innerHTML = self.titlePlaceholder.outerHTML();
            } else {
                if (node.getAttribute('data-introduction') == 1 && node.innerText.trim() == '') {
                    node.innerHTML = self.introductionTextPlaceholder.outerHTML();
                } else {
                    if (node.getAttribute('data-content') == 1 && node.innerText.trim() == '') {
                        node.innerHTML = '<p>' + self.contentPlaceholder.outerHTML() + '</p>';

                        // move cursor
                        var range = document.createRange();
                        var sel = self.options.contentWindow.getSelection();

                        range.setStart(node.firstChild, 0);
                        range.collapse(true);
                        sel.removeAllRanges();
                        sel.addRange(range);
                    }
                }
            }
        });
        return this;
    }

    ViidiumEditor.prototype.checkSelection = function () {
        var newSelection,
            selectionElement;

        if (!this.preventSelectionUpdates && !this.options.disableToolbar) {

            newSelection = this.options.contentWindow.getSelection();
            if ((!this.options.updateOnEmptySelection && newSelection.toString().trim() === '') ||
                (this.options.allowMultiParagraphSelection === false && this.multipleBlockElementsSelected()) ||
                Viidium.Selection.selectionInContentEditableFalse(this.options.contentWindow)) {
                if (!this.options.staticToolbar) {
                    this.hideToolbarActions();
                } else {
                    this.showAndUpdateToolbar();
                }

            } else {
                selectionElement = Viidium.Selection.getSelectionElement(this.options.contentWindow);
                if (!selectionElement || selectionElement.getAttribute('data-disable-toolbar')) {
                    if (!this.options.staticToolbar) {
                        this.hideToolbarActions();
                    }
                } else {
                    this.checkSelectionElement(newSelection, selectionElement);
                }
            }
        }
        return this;
    }

    ViidiumEditor.prototype.bindPaste = function() {
        var i, self = this;
        this.pasteWrapper = function (e) {
            Viidium.pasteHandler.handlePaste(this, e, self.options);
        };
        for (i = 0; i < this.elements.length; i += 1) {
            this.on(this.elements[i], 'paste', this.pasteWrapper);
        }
        return this;
    }

    ViidiumEditor.prototype.onBlockModifier = function(e) {
        var node = Viidium.Selection.getSelectionStart(this.options.ownerDocument);
        var tagName = node.tagName.toLowerCase();
        var isEmpty = /^(\s+|<br\/?>)?$/i;
        var isHeader = /h\d/i;

        if (node.getAttribute('data-placeholder')) {
            e.preventDefault();
            e.stopPropagation();

            return;
        }

        var selection = window.getSelection();
        if (selection.anchorOffset == 0) {
            if (node.getAttribute('data-content') || node.getAttribute('data-title') || node.getAttribute('data-introduction')) {
                e.preventDefault();
                e.stopPropagation();

                return;
            }

            if (tagName === 'p' && node.parentElement.getAttribute('data-content') && node.previousElementSibling === null) {
                e.preventDefault();
                e.stopPropagation();

                return;
            }
        }

        var p;
        if ((e.which === Viidium.Util.keyCode.BACKSPACE || e.which === Viidium.Util.keyCode.ENTER)
            && node.previousElementSibling
                // in a header
            && isHeader.test(tagName)
                // at the very end of the block
            && Viidium.Selection.getCaretOffsets(node).left === 0) {
            if (e.which === Viidium.Util.keyCode.BACKSPACE && isEmpty.test(node.previousElementSibling.innerHTML)) {
                // backspacing the begining of a header into an empty previous element will
                // change the tagName of the current node to prevent one
                // instead delete previous node and cancel the event.
                node.previousElementSibling.parentNode.removeChild(node.previousElementSibling);
                e.preventDefault();
            } else if (e.which === Viidium.Util.keyCode.ENTER) {
                // hitting return in the begining of a header will create empty header elements before the current one
                // instead, make "<p><br></p>" element, which are what happens if you hit return in an empty paragraph
                p = this.options.ownerDocument.createElement('p');
                p.innerHTML = '<br>';
                node.previousElementSibling.parentNode.insertBefore(p, node);
                e.preventDefault();
            }
        } else if (e.which === Viidium.Util.keyCode.DELETE
            && node.nextElementSibling
            && node.previousElementSibling
                // not in a header
            && !isHeader.test(tagName)
                // in an empty tag
            && isEmpty.test(node.innerHTML)
                // when the next tag *is* a header
            && isHeader.test(node.nextElementSibling.tagName)) {
            // hitting delete in an empty element preceding a header, ex:
            //  <p>[CURSOR]</p><h1>Header</h1>
            // Will cause the h1 to become a paragraph.
            // Instead, delete the paragraph node and move the cursor to the begining of the h1

            // remove node and move cursor to start of header
            var range = document.createRange();
            var sel = this.options.contentWindow.getSelection();

            range.setStart(node.nextElementSibling, 0);
            range.collapse(true);

            sel.removeAllRanges();
            sel.addRange(range);

            node.previousElementSibling.parentNode.removeChild(node);

            e.preventDefault();
        }
    }
});