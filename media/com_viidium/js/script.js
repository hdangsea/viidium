if (typeof Viidium === 'undefined') {
    Viidium = {};
}

jQuery(function($) {
    Viidium.page = 0;
    Viidium.containerEle = null;
    Viidium.maxDifference = 1200;
    Viidium.haveMoreItems = true;
    Viidium.limit = 5;

    Viidium.setContainerElement = function(e) {
        Viidium.containerEle = $(e);
        Viidium.fetchMoreItemsUrl = $(e).data('fetch-more-items-url');
        Viidium.limit = $(e).data('limit');
        Viidium.catId = $(e).data('cat-id');
        Viidium.maxDifference = $(e).data('max-difference');
    }

    Viidium.fetchMoreListings = function() {
        var scrollTop = $(document).scrollTop();
        var containerBottom = Viidium.containerEle.offset().top + Viidium.containerEle.height();
        if (containerBottom - scrollTop < Viidium.maxDifference) {
            Viidium.page = Viidium.page + 1;
            Viidium.requestItems();
        }
    }

    Viidium.requestItems = function() {
        if (Viidium.haveMoreItems) {
            $.ajax({
                type: 'POST',
                url: Viidium.fetchMoreItemsUrl,
                data: {page: Viidium.page, limit: Viidium.limit, catId: Viidium.catId},
                success: function (response) {
                    var listings = response.listings;
                    if (listings.length < Viidium.limit) {
                        Viidium.haveMoreItems = false;
                    } else {
                        for (li in listings) {
                            var listing = listings[li];
                            var newListingHtml = jQuery(listing);
                            newListingHtml.find('[data-group]').each(function(idx, e) {
                                var groupId = $(this).data('group');
                                if (typeof Viidium.userGroups[groupId] !== 'undefined') {
                                    var group = Viidium.userGroups[groupId];
                                    $(this).html(group.title);
                                }
                            });

                            Viidium.containerEle.append(newListingHtml);
                        }
                    }
                },
                dataType: 'json'
            })
        }
    }

    Viidium.saveItem = function(published, linkId) {
        var introductionHTML = Viidium.editor.introductionText[0].outerHTML;
        var titleHTML = Viidium.editor.title[0].outerHTML;
        var contentHTML = Viidium.editor.contentText[0].outerHTML;

        var title = $(Viidium.editor.title[0]).text();
        var content = titleHTML + introductionHTML + contentHTML;
        var category = $('[data-category]').val();

        var waitingModal = UIkit.modal('#vi-waiting-dlg');
        waitingModal.show();

        $.ajax(Viidium.saveListingURL, {
            data: {
                link_name: title,
                link_desc: content,
                link_published: published,
                cat_id: category,
                link_id: linkId
            },
            async: false,
            dataType: 'json',
            type: 'POST'
        }).done(function(response) {
            var data = response.data;
            $('#vi-waiting-dlg .vi-msg').html(data.message);
            setTimeout(function() {
                waitingModal.hide();
            }, 1000);

            if (published && data.result) {
                if (typeof data.link_url !== 'undefined') {
                    window.location.href = data.link_url;
                } else {
                    window.location.href = data.user_url;
                }
            }
        });
    }

    Viidium.initEditingPage = function() {
        $('#frmSaveListing').submit(function() {
            var introductionHTML = Viidium.editor.introductionText[0].outerHTML;
            var titleHTML = Viidium.editor.title[0].outerHTML;
            var contentHTML = Viidium.editor.contentText[0].outerHTML;

            var title = $(Viidium.editor.title[0]).text();
            var content = titleHTML + introductionHTML + contentHTML;

            $(this).find('[name=link_name]').val(title);
            $(this).find('[name=link_desc]').val(content);

            var waitingModal = UIkit.modal('#vi-waiting-dlg');
            waitingModal.show();

            return true;
        });

        Viidium.editor = new ViidiumEditor();
        Viidium.editor.init('.vi-editable', {
            buttonLabels: 'fontawesome',
            buttons: ['bold', 'italic', 'underline', 'anchor', 'header1', 'header2', 'quote', 'justifyCenter', 'justifyFull', 'justifyLeft', 'justifyRight'],
            placeholder: '',
            firstHeader: 'h2',
            secondHeader: 'h3',
            postTitle: Viidium.postTitle,
            postIntroduction: Viidium.postIntroduction,
            postContent: Viidium.postContent
        });

        Viidium.insertEditor = $('.vi-editable').mediumInsert({
            editor: Viidium.editor,
            addons: {
                extimages: {
                    uploadScript: Viidium.uploadImageUrl,
                    deleteScript: Viidium.deleteImageUrl
                },
                coverimage : {
                    initAddonButton: false,
                    image: Viidium.coverPhotoUrl
                },
                embeds: {},
                images: false
            }
        });

        $(Viidium.insertEditor).find('.medium-insert-action').on('click', function() {
            $(Viidium.insertEditor).find('[data-content=1]').append(MediumInsert.Templates['src/js/templates/core-empty-line.hbs']());
        });
    }

    $(document).ready(function() {
        if (Viidium.containerEle) {
            var timer;
            var fetching = false;
            $(window).scroll(function () {
                if (timer) {
                    clearTimeout(timer);
                }

                timer = setTimeout(function () {
                    if (!fetching) {
                        fetching = true;
                        Viidium.fetchMoreListings();
                        fetching = false;
                    }
                }, 200);
            });
        }
    });
});