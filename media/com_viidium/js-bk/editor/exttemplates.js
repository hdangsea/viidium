this["MediumInsert"] = this["MediumInsert"] || {};
this["MediumInsert"]["Templates"] = this["MediumInsert"]["Templates"] || {};

//this["MediumInsert"]["Templates"]["src/js/templates/extimages-fileupload.hbs"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
//  return "<input type=\"file\" multiple>";
//  },"useData":true});

this["MediumInsert"]["Templates"]["src/js/templates/extimages-fileupload.hbs"] = Handlebars.compile(
    '<input type="file" multiple>'
);

//this["MediumInsert"]["Templates"]["src/js/templates/extimages-image.hbs"] = Handlebars.template({
//    "1":function(depth0,helpers,partials,data) {
//        return "        <div class=\"medium-insert-images-progress\"></div>\n";
//    },
//    "compiler":[6,">= 2.0.0-beta.1"],
//    "main":function(depth0,helpers,partials,data) {
//        var stack1,
//            helper,
//            functionType="function",
//            helperMissing=helpers.helperMissing,
//            escapeExpression=this.escapeExpression,
//            buffer = "<figure contenteditable=\"false\">\n    <img src=\""
//                + escapeExpression(((helper = (helper = helpers.img || (depth0 != null ? depth0.img : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"img","hash":{},"data":data}) : helper)))
//                + "\" alt=\"\">\n";
//            stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.progress : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
//  if (stack1 != null) {
//      buffer += stack1;
//  }
//
//  return buffer + "</figure>";
//},"useData":true});

this["MediumInsert"]["Templates"]["src/js/templates/extimages-image.hbs"] = Handlebars.compile(
    '<figure contenteditable="false">' +
        '<div class="extimage-place-holder">' +
            '<img src="{{img}}" alt="">' +
        '</div>' +
        '<figcaption class="extimage-caption" contenteditable="true" data-default-value="Type caption for image (optional)" data-value=""></figcaption>' +
        '{{#if progress }}' +
            '<div class="medium-insert-extimages-progress"></div>' +
        '{{/if}}' +
    '</figure>'
);

//this["MediumInsert"]["Templates"]["src/js/templates/extimages-progressbar.hbs"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
//  return "<progress min=\"0\" max=\"100\" value=\"0\">0</progress>";
//  },"useData":true});

this["MediumInsert"]["Templates"]["src/js/templates/extimages-progressbar.hbs"] = Handlebars.compile(
    '<progress min="0" max="100" value="0">0</progress>'
);

//this["MediumInsert"]["Templates"]["src/js/templates/extimages-toolbar.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
//  var stack1, buffer = "";
//  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.label : depth0), {"name":"if","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
//  if (stack1 != null) { buffer += stack1; }
//  return buffer;
//},"2":function(depth0,helpers,partials,data) {
//  var stack1, helper, lambda=this.lambda, escapeExpression=this.escapeExpression, functionType="function", helperMissing=helpers.helperMissing, buffer = "                <li>\n                    <button class=\"medium-editor-action\" data-action=\""
//    + escapeExpression(lambda((data && data.key), depth0))
//    + "\">";
//  stack1 = ((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"label","hash":{},"data":data}) : helper));
//  if (stack1 != null) { buffer += stack1; }
//  return buffer + "</button>\n                </li>\n";
//},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
//  var stack1, buffer = "<div class=\"medium-insert-images-toolbar medium-editor-toolbar medium-toolbar-arrow-under medium-editor-toolbar-active\">\n    <ul class=\"medium-editor-toolbar-actions clearfix\">\n";
//  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.styles : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
//  if (stack1 != null) { buffer += stack1; }
//  return buffer + "    </ul>\n</div>";
//},"useData":true});

this["MediumInsert"]["Templates"]["src/js/templates/extimages-toolbar.hbs"] = Handlebars.compile(
    '<div class="medium-insert-extimages-toolbar medium-editor-toolbar medium-toolbar-arrow-under medium-editor-toolbar-active">' +
        '<ul class="medium-editor-toolbar-actions clearfix">' +
        '{{#each styles}}' +
            '{{#if label}}' +
            '<li>' +
                '<button class="medium-editor-action" data-action="{{@key}}">{{{label}}}</button>' +
            '</li>' +
            '{{/if}}' +
        '{{/each}}' +
        '</ul>' +
    '</div>'
);