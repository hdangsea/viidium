jQuery(function($) {
    function extend(b, a) {
        var prop;
        if (b === undefined) {
            return a;
        }
        for (prop in a) {
            if (a.hasOwnProperty(prop) && b.hasOwnProperty(prop) === false) {
                b[prop] = a[prop];
            }
        }
        return b;
    }

    function isDescendant(parent, child) {
        var node = child.parentNode;
        while (node !== null) {
            if (node === parent) {
                return true;
            }
            node = node.parentNode;
        }
        return false;
    }

    // http://stackoverflow.com/questions/5605401/insert-link-in-contenteditable-element
    // by Tim Down
    function saveSelection() {
        var i,
            len,
            ranges,
            sel = this.options.contentWindow.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            ranges = [];
            for (i = 0, len = sel.rangeCount; i < len; i += 1) {
                ranges.push(sel.getRangeAt(i));
            }
            return ranges;
        }
        return null;
    }

    function restoreSelection(savedSel) {
        var i,
            len,
            sel = this.options.contentWindow.getSelection();
        if (savedSel) {
            sel.removeAllRanges();
            for (i = 0, len = savedSel.length; i < len; i += 1) {
                sel.addRange(savedSel[i]);
            }
        }
    }

    // http://stackoverflow.com/questions/1197401/how-can-i-get-the-element-the-caret-is-in-with-javascript-when-using-contentedi
    // by You
    function getSelectionStart() {
        var node = this.options.ownerDocument.getSelection().anchorNode,
            startNode = (node && node.nodeType === 3 ? node.parentNode : node);
        return startNode;
    }

    // http://stackoverflow.com/questions/4176923/html-of-selected-text
    // by Tim Down
    function getSelectionHtml() {
        var i,
            html = '',
            sel,
            len,
            container;
        if (this.options.contentWindow.getSelection !== undefined) {
            sel = this.options.contentWindow.getSelection();
            if (sel.rangeCount) {
                container = this.options.ownerDocument.createElement('div');
                for (i = 0, len = sel.rangeCount; i < len; i += 1) {
                    container.appendChild(sel.getRangeAt(i).cloneContents());
                }
                html = container.innerHTML;
            }
        } else if (this.options.ownerDocument.selection !== undefined) {
            if (this.options.ownerDocument.selection.type === 'Text') {
                html = this.options.ownerDocument.selection.createRange().htmlText;
            }
        }
        return html;
    }

    // https://github.com/jashkenas/underscore
    function isElement(obj) {
        return !!(obj && obj.nodeType === 1);
    }

    // http://stackoverflow.com/questions/6690752/insert-html-at-caret-in-a-contenteditable-div
    function insertHTMLCommand(doc, html) {
        var selection, range, el, fragment, node, lastNode;

        if (doc.queryCommandSupported('insertHTML')) {
            return doc.execCommand('insertHTML', false, html);
        }

        selection = window.getSelection();
        if (selection.getRangeAt && selection.rangeCount) {
            range = selection.getRangeAt(0);
            range.deleteContents();

            el = doc.createElement("div");
            el.innerHTML = html;
            fragment = doc.createDocumentFragment();
            while (el.firstChild) {
                node = el.firstChild;
                lastNode = fragment.appendChild(node);
            }
            range.insertNode(fragment);

            // Preserve the selection:
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                range.collapse(true);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }
    }

    ViidiumEditor = function() {}

    ViidiumEditor.prototype = new MediumEditor;
    ViidiumEditor.prototype.defaults = extend(MediumEditor.prototype.defaults, {
        postContent: '',
        postIntroduction: '',
        postTitle: ''
    });
    ViidiumEditor.constructor = ViidiumEditor;

    ViidiumEditor.prototype.init = function(elements, options) {
        MediumEditor.prototype.init.call(this, elements, options);

        // content block
        if (options.postContent == '') {
            this.contentPlaceholder = $('<span/>', {
                text: 'Type your content here',
                class: 'placeholder-content',
                'data-placeholder': 1
            });
            this.contentText = $('<div/>', {
                id: 'vi-post-content',
                class: 'content',
                'data-content': 1
                //'data-medium-element': 1
            });
            this.contentText.append('<p></p>');
            this.contentText.children().append(this.contentPlaceholder);
        } else {
            this.contentText = $(options.postContent);
        }
        $(this.elements).prepend(this.contentText);

        // introduction block
        if (options.postIntroduction == '') {
            this.introductionTextPlaceholder = $('<span/>', {
                text: 'Please type the introduction text',
                class: 'placeholder-introduction-text',
                'data-placeholder': 1
            });
            this.introductionText = $('<' + this.options.secondHeader + '/>', {
                id: 'vi-post-introduction',
                'data-introduction': 1,
                'style': 'text-align:center',
                'class': 'vi-introduction-text',
                'data-disable-toolbar': 1
            });
            this.introductionText.append(this.introductionTextPlaceholder);
        } else {
            this.introductionText = $(options.postIntroduction);
        }
        $(this.elements).prepend(this.introductionText);

        // title block
        if (options.postTitle == '') {
            this.titlePlaceholder = $('<span/>', {
                text: 'Title',
                class: 'placeholder-title',
                'data-placeholder': 1
            });
            this.title = $('<' + this.options.firstHeader + '/>', {
                'data-title': 1,
                'style': 'text-align:center',
                'id': 'vi-post-title',
                'data-disable-toolbar': 1
            });
            this.title.append(this.titlePlaceholder);
        } else {
            this.title = $(options.postTitle)
        }
        $(this.elements).prepend(this.title);

        this.elements[0].focus();
    };

    ViidiumEditor.prototype.bindParagraphCreation = function(index) {
        var self = this;
        this.on(this.elements[index], 'keypress', function (e) {
            var node,
                tagName;

            if (e.which === 32) {
                node = getSelectionStart.call(self);
                tagName = node.tagName.toLowerCase();
                if (tagName === 'a') {
                    document.execCommand('unlink', false, null);
                }
            }

            node = getSelectionStart.call(self);
            if ($(node).data('placeholder') == 1) {
                $(node).remove();
            }
        });

        this.on(this.elements[index], 'keyup', function (e) {
            var node = getSelectionStart.call(self),
                tagName,
                editorElement;

            if (node && node.getAttribute('data-medium-element') && node.children.length === 0 && !(self.options.disableReturn || node.getAttribute('data-disable-return'))) {
                document.execCommand('formatBlock', false, 'p');
            }

            if ($(node).data('title') == 1 && node.innerText.trim() == '') {
                $(node).html(self.titlePlaceHolder);
            } else {
                if ($(node).data('introduction') == 1 && node.innerText.trim() == '') {
                    $(node).html(self.introductionTextPlaceholder);
                }
            }

            if (e.which === 13) {
                node = getSelectionStart.call(self);
                tagName = node.tagName.toLowerCase();
                editorElement = self.getSelectionElement();

                if (!(self.options.disableReturn || editorElement.getAttribute('data-disable-return')) &&
                    tagName !== 'li' && !self.isListItemChild(node)) {
                    if (!e.shiftKey) {
                        document.execCommand('formatBlock', false, 'p');
                    }
                    if (tagName === 'a') {
                        document.execCommand('unlink', false, null);
                    }
                }
            }
        });
        return this;
    }

    ViidiumEditor.prototype.findMatchingSelectionParent = function(testElementFunction) {
        var selection = this.options.contentWindow.getSelection();

        if (selection.rangeCount === 0) {
            return false;
        }

        var range = selection.getRangeAt(0);
        var startNode = range.startContainer.parentNode;
        var endNode = range.endContainer.parentNode;

        if (startNode.getAttribute('data-title') || startNode.getAttribute('data-introduction')
            || endNode.getAttribute('data-title') || endNode.getAttribute('data-introduction')) {
            return false;
        }

        var current = range.commonAncestorContainer;

        do {
            if (current.nodeType === 1){
                if ( testElementFunction(current) )
                {
                    return current;
                }

                //do not traverse upwards past the nearest containing editor
                if (current.getAttribute('data-medium-element')) {
                    return false;
                }
            }

            current = current.parentNode;
        } while (current);

        return false;
    }
});