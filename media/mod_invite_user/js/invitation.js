jQuery(function ($) {
        $("#invite-form").submit(function (event) {
            event.preventDefault();
            var $form = $(this);
            var term = $form.serialize();
            var url = $form.attr("action");
            var posting = $.post(url, term);
            posting.done(function (data) {
                var result = $.parseJSON(data);
                if(result.remaining_invitation>0)
                    $("#number-of-invitation").html("You have <b>" + result.remaining_invitation + "</b> remaining invitation(s)" + "<br /><div style='color:red'>" + result.message + "</div>");
                else
                    $("#number-of-invitation").html("<div style='color:red'>You have <b>" + result.remaining_invitation + "</b> remaining invitation(s)</div>")
            });
        });
    }
);