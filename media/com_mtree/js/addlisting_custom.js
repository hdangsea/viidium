jQuery(function($){
    markdownEditor = jQuery.UIkit.htmleditor(jQuery('textarea'), {markdown: true});
    var progressbar = $("#progressbar"),
        bar         = progressbar.find('.uk-progress-bar'),
        settings    = {
            action: "/", // upload url $("#mtForm").data('upload-url')
            //param: 'image[]',
            allow : '*.(jpg|jpeg|gif|png)', // allow only images

            loadstart: function() {
                bar.css("width", "0%").text("0%");
                progressbar.removeClass("uk-hidden");
            },

            progress: function(percent) {
                percent = Math.ceil(percent);
                bar.css("width", percent+"%").text(percent+"%");
            },

            beforeAll: function(files) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#uploadPreview').attr('src', e.target.result);
                }

                reader.readAsDataURL(files[0]);
            },

            allcomplete: function(response) {

                bar.css("width", "100%").text("100%");

                setTimeout(function(){
                    progressbar.addClass("uk-hidden");
                }, 250);

            }
        },
        editor_settings    = {

            action: $("#mtForm").data('upload-url'), // upload url
            param: 'image[]',
            allow : '*.(jpg|jpeg|gif|png)', // allow only images

            loadstart: function() {
                bar.css("width", "0%").text("0%");
                progressbar.removeClass("uk-hidden");
            },

            progress: function(percent) {
                percent = Math.ceil(percent);
                bar.css("width", percent+"%").text(percent+"%");
            },

            allcomplete: function(response) {
                bar.css("width", "100%").text("100%");

                setTimeout(function(){
                    progressbar.addClass("uk-hidden");
                }, 250);

                var result = $.parseJSON(response);
                if (result.filepath) {
                    markdownEditor.replaceSelection('![](' + result.filepath + ')');
                }
            }
        };

    var select = $.UIkit.uploadSelect($("#upload-select"), settings);
    var drop = $.UIkit.uploadDrop($("#upload-drop"), settings);
    var editor = $.UIkit.uploadDrop($("#upload-drop-editor"), editor_settings);
});

function submitbutton_custom(pressbutton){
    var validation_all_passed=true;
    var scroll = new Fx.SmoothScroll({links:'mtForm',wheelStops:false})
    var first_invalidated_input_id=0;
    var first_invalidated_input_element;

    jQuery('#mtForm .controls input[id^="cf"]').each(function(index,el){
        var target=el;
        var validation_failed=false;
        var id=getCfId(target.id);
        var field_id=getCfId(target.id);
        if(!mtValidate(el)){
            validation_failed=true;
            validation_all_passed=false;
            mtShowAdvice(target.id);
        }
        if(target.required && !mtValidateIsEmpty(target)){
            validation_failed=true;
            validation_all_passed=false;
        }
        if(validation_failed){
            addValidationErrorHighlight(field_id);
            if(first_invalidated_input_id==0){
                first_invalidated_input_id=field_id;
                first_invalidated_input_element=el;
            }
        }else{
            mtRemoveAdvice(id);
            removeValidationErrorHighlight(field_id);
        }
    });
    if(validation_all_passed && jQuery("input[name='cat_id']").val() !=0){
        var form = document.mtForm;

        var imageFileEle = document.getElementById("upload-select");
        var ckKeepImage = jQuery('#ckKeepImage');

        if (imageFileEle.files.length > 0) {
            if (ckKeepImage.length > 0) {
                ckKeepImage.attr('checked', true);
            }
        } else {
            var ckRemoveImage = jQuery('#ckRemoveImage');
            if (ckRemoveImage.length > 0) {
                ckKeepImage.attr('checked', !ckRemoveImage.attr('checked'));
            }
        }

        Joomla.submitform(pressbutton, document.getElementById('mtForm'));
    } else {
        jQuery("#warning-choose-category").css("display","inherit");
        if(jQuery("input[name='cat_id']").val() ==0) {
            scroll.toElement(document.getElementById('linkcats'));
        }
        else {
            var li_element = document.getElementById('field_' + first_invalidated_input_id);
            scroll.toElement(li_element);
            first_invalidated_input_element.focus();
            first_invalidated_input_element.select();
        }
    }
}