<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_viidium
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$params = JComponentHelper::getParams('com_viidium');

require( JPATH_ROOT.'/components/com_mtree/init.php');
require_once JPATH_COMPONENT . '/helpers/media.php';
require_once JPATH_COMPONENT . '/helpers/lib.php';
require_once JPATH_COMPONENT . '/helpers/common.php';

require_once JPATH_ADMINISTRATOR.'/components/com_mtree/admin.mtree.class.php';
require_once JPATH_SITE .'/components/com_mtree/mtree.class.php';
require_once JPATH_SITE . '/components/com_mtree/mtree.tools.php';
require_once JPATH_SITE . '/components/com_mtree/methods.php';

define('COM_VIIDIUM_BASE',    JPATH_ROOT . '/listings');

$controller	= JControllerLegacy::getInstance('Viidium', array('base_path' => JPATH_COMPONENT));
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();