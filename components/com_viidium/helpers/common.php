<?php

defined('_JEXEC') or die;

class ViidiumHelper
{
    CONST  CODE_CHARACTERS="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

    public static function randChars($c, $l, $u = FALSE) {
        if (!$u) for ($s = '', $i = 0, $z = strlen($c)-1; $i < $l; $x = rand(0,$z), $s .= $c{$x}, $i++);
        else for ($i = 0, $z = strlen($c)-1, $s = $c{rand(0,$z)}, $i = 1; $i != $l; $x = rand(0,$z), $s .= $c{$x}, $s = ($s{$i} == $s{$i-1} ? substr($s,0,-1) : $s), $i=strlen($s));
        return $s;
    }

    public static function getRemainingInvitation(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(array('remaining_invitation')));
        $query->from($db->quoteName('#__vd_users'));
        $query->where($db->quoteName('user_id') . ' = '. JFactory::getUser()->id);
        $db->setQuery($query);
        $db->execute();
        $number=$db->loadObjectList();
        if(!isset($number[0]) || $number[0]->remaining_invitation<=0)
        {
            $data = array('result' => 0, 'remaining_invitation' => 0 ,"message" => JText::_("COM_VIIDIUM_ZERO_INVITATION"));
            echo json_encode($data);
            JFactory::getApplication()->close();
        }
        return $number;
    }
}
