<?php

defined('_JEXEC') or die;

abstract class ViidiumMediaHelper
{
	public static function isImage($fileName)
	{
		JLog::add('MediaHelper::isImage() is deprecated. Use ViidiumHelperMedia::isImage() instead.', JLog::WARNING, 'deprecated');
		$mediaHelper = new ViidiumHelperLib;

		return $mediaHelper->isImage($fileName);
	}

	public static function getTypeIcon($fileName)
	{
		JLog::add('MediaHelper::getTypeIcon() is deprecated. Use ViidiumHelperMedia::getTypeIcon() instead.', JLog::WARNING, 'deprecated');
		$mediaHelper = new ViidiumHelperLib;

		return $mediaHelper->getTypeIcon($fileName);
	}

	public static function canUpload($file, $err = '')
	{
		JLog::add('MediaHelper::canUpload() is deprecated. Use ViidiumHelperMedia::canUpload() instead.', JLog::WARNING, 'deprecated');
		$mediaHelper = new ViidiumHelperLib;
		return $mediaHelper->canUpload($file, 'com_viidium');
	}

	public static function parseSize($size)
	{
		JLog::add('MediaHelper::parseSize() is deprecated. Use JHtmlNumber::bytes() instead.', JLog::WARNING, 'deprecated');
		return JHtml::_('number.bytes', $size);
	}

	public static function imageResize($width, $height, $target)
	{
		JLog::add('MediaHelper::countFiles() is deprecated. Use ViidiumHelperMedia::countFiles() instead.', JLog::WARNING, 'deprecated');
		$mediaHelper = new ViidiumHelperLib;

		return $mediaHelper->imageResize($width, $height, $target);
	}

	public static function countFiles($dir)
	{
		JLog::add('MediaHelper::countFiles() is deprecated. Use ViidiumHelperMedia::countFiles() instead.', JLog::WARNING, 'deprecated');
		$mediaHelper = new ViidiumHelperLib;

		return $mediaHelper->countFiles($dir);
	}
}
