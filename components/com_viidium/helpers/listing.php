<?php
defined('_JEXEC') or die;

abstract class ViidiumListingHelper {
    public static function getListings($catId, $limitStart = 0, $limit = 5) {
        require_once JPATH_SITE . '/components/com_mtree/init.php';
        require_once JPATH_ADMINISTRATOR . '/components/com_mtree/mfields.class.php';
        require_once JPATH_ADMINISTRATOR . '/components/com_mtree/admin.mtree.class.php';

        require_once JPATH_SITE . '/components/com_mtree/mtree.class.php';
        require_once JPATH_SITE . '/components/com_mtree/mtree.tools.php';
        require_once JPATH_SITE . '/components/com_mtree/methods.php';

        global $mtconf;

        $input = JFactory::getApplication()->input;
        $sort = $input->getCmd('sort', $mtconf->get('all_listings_sort_by'));

        require_once JPATH_SITE.'/components/com_mtree/listlisting.php';

        $listListing = new mtListListing('listnew');
        $listListing->setLimitStart($limitStart);
        $listListing->setLimit($limit);
        $listListing->setSubcats($catId);
        $listListing->setSort($sort);
        $listings = $listListing->getListings();

        foreach ($listings as $listing) {
            if ($listing->link_image) {
                $listing->image_path = $mtconf->getjconf('live_site')
                    . $mtconf->get('relative_path_to_listing_small_image') . $listing->link_image;
            }
        }

        return $listings;
    }
}