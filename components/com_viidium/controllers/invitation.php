<?php
defined('_JEXEC') or die;

class ViidiumControllerInvitation extends JControllerLegacy
{
    public function invite()
    {
        JSession::checkToken('request') or jexit(JText::_('JINVALID_TOKEN'));
        $input = JFactory::getApplication()->input;
        $email = $input->getString("email", "");

        //get remaining_invitation
        $numberInvite=ViidiumHelper::getRemainingInvitation();

        if (!isset($email) || $email == "") {
            $data = array('result' => 0, 'remaining_invitation' => $numberInvite[0]->remaining_invitation, 'message' => JText::_('COM_VIIDIUM_INVALID_EMAIL'));
            echo json_encode($data);
            JFactory::getApplication()->close();
        }



        //random code
        $code = ViidiumHelper::randChars(ViidiumHelper::CODE_CHARACTERS, 16);

        //send mail to recipient
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array(
            $config->get('mailfrom'),
            $config->get('fromname'));
        $mailer->setSender($sender);
        $mailer->addRecipient($email);
        $body = JText::sprintf('COM_VIIDIUM_TEMPLATE_EMAIL', JUri::base() . "?option=com_users&view=registration&code=$code");
        $mailer->setSubject('Viidium');
        $mailer->setBody($body);
        $send = $mailer->Send();
        if ($send !== true) {
            $data = array('result' => 0, 'remaining_invitation' => $numberInvite[0]->remaining_invitation, "message" => JText::_("COM_VIIDIUM_SEND_MAIL_ERROR_MESSAGE"));
            echo json_encode($data);
            JFactory::getApplication()->close();
        }

        //update remaining_invitation
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $fields = array(
            $db->quoteName('remaining_invitation') . ' = ' . $db->quoteName('remaining_invitation') . " - 1"
        );
        $conditions = array(
            $db->quoteName('user_id') . ' = ' . JFactory::getUser()->id
        );
        $query->update($db->quoteName('#__vd_users'))->set($fields)->where($conditions);
        $db->setQuery($query);
        $result = $db->execute();

        //insert data table vd_invitations
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $columns = array('user_id', 'email', 'code');
        $values = array(intval(JFactory::getUser()->id), "'" . $email . "'", "'" . $code . "'");
        $query
            ->insert($db->quoteName('#__vd_invitations'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));
        $db->setQuery($query);
        $db->execute();

        //get remaining_invitation
        $number = ViidiumHelper::getRemainingInvitation();
        $data = array('result' => 1, 'remaining_invitation' => $number[0]->remaining_invitation, "message" => JText::_("COM_VIIDIUM_INVITE_SUCCESS"));
        echo json_encode($data);
        JFactory::getApplication()->close();
    }

}
