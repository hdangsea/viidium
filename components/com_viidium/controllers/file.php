<?php


defined('_JEXEC') or die;

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');


class ViidiumControllerFile extends JControllerLegacy
{
    const FOLDER_LISTING_UPLOAD = 'listings';

    /**
     * Upload one or more files
     *
     * @return  boolean
     *
     * @since   1.5
     */
    public function upload() {
        // Check for request forgeries
        JSession::checkToken('request') or jexit(JText::_('JINVALID_TOKEN'));
        $params = JComponentHelper::getParams('com_viidium');

        // Get some data from the request
        $files = $this->input->files->get('files', '', 'array');
        $folder = $this->input->get('folder', '', 'path');

        // Total length of post back data in bytes.
        $contentLength = (int)$_SERVER['CONTENT_LENGTH'];

        // Instantiate the media helper
        $mediaHelper = new ViidiumHelperLib;

        // Maximum allowed size of post back data in MB.
        $postMaxSize = $mediaHelper->toBytes(ini_get('post_max_size'));

        // Maximum allowed size of script execution in MB.
        $memoryLimit = $mediaHelper->toBytes(ini_get('memory_limit'));

        // Check for the total size of post back data.
        if (($postMaxSize > 0 && $contentLength > $postMaxSize)
            || ($memoryLimit != -1 && $contentLength > $memoryLimit)) {
            $data = array('result' => 0, 'filepath' => '', 'error' => JText::_('COM_VIIDIUM_ERROR_WARNUPLOADTOOLARGE'));
            echo json_encode($data);
            JFactory::getApplication()->close();
        }

        $uploadMaxSize = $params->get('upload_maxsize', 0) * 1024 * 1024;
        $uploadMaxFileSize = $mediaHelper->toBytes(ini_get('upload_max_filesize'));

        // Perform basic checks on file info before attempting anything
        foreach ($files as &$file) {
            //$file['name']     = JFile::makeSafe($file['name']);
            $filetype = explode(".", JFile::makeSafe($file['name']));
            $file['name'] = JFactory::getUser()->id . '-' . time() . '.' . $filetype[count($filetype) - 1];
            $file['filepath'] = JPath::clean(implode(DIRECTORY_SEPARATOR,
                array(JPATH_ROOT . DIRECTORY_SEPARATOR . self::FOLDER_LISTING_UPLOAD, $folder, $file['name'])));

            if (($file['error'] == 1)
                || ($uploadMaxSize > 0 && $file['size'] > $uploadMaxSize)
                || ($uploadMaxFileSize > 0 && $file['size'] > $uploadMaxFileSize)) {
                // File size exceed either 'upload_max_filesize' or 'upload_maxsize'.
                $data = array('result' => 0, 'filepath' => '', 'error' => JText::_('COM_VIIDIUM_ERROR_WARNFILETOOLARGE'));
                echo json_encode($data);
                JFactory::getApplication()->close();
            }

            if (JFile::exists($file['filepath'])) {
                // A file with this name already exists
                $data = array('result' => 0, 'filepath' => '', 'error' => JText::_('COM_VIIDIUM_ERROR_FILE_EXISTS'));
                echo json_encode($data);
                JFactory::getApplication()->close();
            }

            if (!isset($file['name'])) {
                // No filename (after the name was cleaned by JFile::makeSafe)
                $data = array('result' => 0, 'filepath' => '', 'error' => JText::_('COM_VIIDIUM_INVALID_REQUEST'));
                echo json_encode($data);
                JFactory::getApplication()->close();
            }
        }

        // Set FTP credentials, if given
        JClientHelper::setCredentialsFromRequest('ftp');
        JPluginHelper::importPlugin('content');
        $dispatcher = JEventDispatcher::getInstance();

        foreach ($files as &$file) {
            // The request is valid
            $err = null;

            if (!ViidiumMediaHelper::canUpload($file, $err)) {
                // The file can't be uploaded
                $data = array('result' => 0, 'filepath' => '');
                echo json_encode($data);
                JFactory::getApplication()->close();
            }

            // Trigger the onContentBeforeSave event.
            $object_file = new JObject($file);
            $result = $dispatcher->trigger('onContentBeforeSave', array('com_viidium.file', &$object_file, true));

            if (in_array(false, $result, true)) {
                // There are some errors in the plugins
                $data = array('result' => 0, 'filepath' => '', 'error' => JText::plural('COM_VIIDIUM_ERROR_BEFORE_SAVE', count($errors = $object_file->getErrors()), implode('<br />', $errors)));
                echo json_encode($data);
                JFactory::getApplication()->close();
            }

//            list($width, $height) = getimagesize($object_file->tmp_name);
//            if ($width < 300 || $height < 300) {
//                $data = array('result' => 0, 'filepath' => '', 'error' => JText::_('INVALID SIZE'));
//                echo json_encode($data);
//                JFactory::getApplication()->close();
//            }

            if (!JFile::upload($object_file->tmp_name, $object_file->filepath)) {
                // Error in upload
                $data = array('result' => 0, 'filepath' => '', 'error' => JText::_('COM_VIIDIUM_ERROR_UNABLE_TO_UPLOAD_FILE'));
                echo json_encode($data);
                JFactory::getApplication()->close();
            } else {
                // Trigger the onContentAfterSave event.
                $dispatcher->trigger('onContentAfterSave', array('com_viidium.file', &$object_file, true));
                $this->setMessage(JText::sprintf('COM_VIIDIUM_UPLOAD_COMPLETE', substr($object_file->filepath, strlen(COM_VIIDIUM_BASE))));
            }
        }

        $data = array(
            'result' => 1,
            'files' => array(
                array('url' => JUri::base() . self::FOLDER_LISTING_UPLOAD . '/' . $object_file->name)
            )
        );
        echo json_encode($data);
        JFactory::getApplication()->close();
    }

    public function delete() {
        // Check for request forgeries
        JSession::checkToken('request') or jexit(JText::_('JINVALID_TOKEN'));
        $user = JFactory::getUser();

        if ($user->id) {
            $input = JFactory::getApplication()->input;
            $file = $input->getString('file', '');
            $folder = $this->input->get('folder', '', 'path');

            if (!empty($file)) {
                $urlListingsFolder = JUri::root() . self::FOLDER_LISTING_UPLOAD . '/';
                if (strpos($file, $urlListingsFolder) === 0) {
                    $fileName = substr($file, strlen($urlListingsFolder));
                    $tokens = explode('-', $fileName);
                    if (count($tokens) > 1) {
                        $fileUserId = $tokens[0];
                    }

                    $canDelete = ($fileUserId == $user->id) || $user->authorise('listing.edit', 'com_viidium');
                    if ($canDelete) {
                        JPluginHelper::importPlugin('content');
                        $dispatcher	= JEventDispatcher::getInstance();
                        $fullPath = JPath::clean(implode(DIRECTORY_SEPARATOR, array(JPATH_BASE, self::FOLDER_LISTING_UPLOAD, $folder, $fileName)));
                        $fileObj = new JObject(array('filepath' => $fullPath));
                        $ret = true;
                        if (is_file($fullPath)) {
                            // Trigger the onContentBeforeDelete event.
                            $result = $dispatcher->trigger('onContentBeforeDelete', array('com_viidium.file', &$fileObj));

                            if (in_array(false, $result, true)) {
                                // There are some errors in the plugins
                                JError::raiseWarning(100, JText::plural('COM_VIIDIUM_ERROR_BEFORE_DELETE',
                                    count($errors = $fileObj->getErrors()), implode('<br />', $errors)));
                            }

                            $ret &= JFile::delete($fileObj->filepath);

                            // Trigger the onContentAfterDelete event.
                            $dispatcher->trigger('onContentAfterDelete', array('com_viidium.file', &$fileObj));
                            $this->setMessage(JText::sprintf('COM_VIIDIUM_DELETE_COMPLETE', $file));

                            $data = array(
                                'result' => $ret,
                                'message' => JText::sprintf('COM_VIIDIUM_DELETE_COMPLETE', $file)
                            );
                            echo json_encode($data);
                            JFactory::getApplication()->close();
                        }
                    }
                }
            }
        }
    }
}
