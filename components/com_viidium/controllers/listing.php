<?php
defined('_JEXEC') or die;

class ViidiumControllerListing extends JControllerLegacy {
    public function save() {
        global $mtconf;
        $input = JFactory::getApplication()->input;
        $user = JFactory::getUser();

        $catId = $input->getInt('cat_id', 0);
        if ($catId === 0) {
            return false;
        }

        $linkId = $input->getInt('link_id', 0);

        if (($mtconf->get('user_addlisting') == 1 && $user->id < 1 && $linkId == 0)
            || ($mtconf->get('user_addlisting') == -1 && $linkId == 0)
            || ($mtconf->get('user_allowmodify') == 0 && $linkId > 0)) {
            JError::raiseError( 403, JText::_( 'JERROR_ALERTNOAUTHOR' ) );
        }

        $db	= JFactory::getDbo();
        $row = new mtLinks($db);

        $isNew = true;
        if ($linkId > 0) {
            $query = $db->getQuery(true);
            $query->select('*');
            $query->from($db->quoteName('#__mt_links', 'l'));
            $query->where('l.link_id = ' . $db->quote($linkId));
            $db->setQuery($query);
            $arrData = $db->loadAssoc();
            foreach ($arrData as $key => $value) {
                $row->$key = $value;
            }
            $isNew = false;
        }

        $post = $_POST;
        $row->bind($post);
        $mtconf->setCategory($row->cat_id);
        $jdate = JFactory::getDate();
        if ($isNew) {
            $row->link_created = $jdate->toSql();
            $row->publish_up = $jdate->toSql();
            $row->ordering = 999;

            // Set an expire date for listing if enabled in configuration
            if ($mtconf->get('days_to_expire') > 0) {
                $jdate->add(new DateInterval('P'.intval($mtconf->get('days_to_expire')).'D'));
                $row->publish_down  = $jdate->toSql(true);
            }

            if ($user->id > 0) {
                $row->user_id = $user->id;
            } else {
                jimport('joomla.access.access');
                $superUsers = JAccess::getUsersByGroup(8);
                $row->user_id = $superUsers[0];
            }
        } else {
            $row->link_modified = $jdate->toSql(true);

            $db->setQuery("SELECT cat_id FROM #__mt_cl AS cl WHERE link_id ='".$row->link_id."' AND main = 1 LIMIT 1" );
            $oldState = $db->loadObject();
            if ($row->cat_id <> $oldState->cat_id) {
                $row->updateLinkCount(1);
                $row->updateLinkCount(-1, $oldState->cat_id);
            }
        }

        if (empty($row->alias)){
            $row->alias = JFilterOutput::stringURLSafe($row->link_name);
        }

        // Approval for adding listing
        if ( $mtconf->get('needapproval_addlisting') ) {
            $row->link_approved = '0';
        } else {
            $row->link_approved = 1;
            $row->link_published = 1;
            $row->updateLinkCount(1);
            $cache = JFactory::getCache('com_mtree');
            $cache->clean();
        }

        $row->store();

        $files = $_FILES;
        if (isset($files['cover_photo'])) {
            $file = $files['cover_photo'];
        }
        $removedFiles = array();

        if ((!$isNew && !empty($file['name'])) || ($input->getInt('remove_image', 0))) {
            $query = $db->getQuery(true);
            $query->select('*');
            $query->from($db->quoteName('#__mt_images'));
            $query->where('link_id = ' . $linkId);
            $db->setQuery($query);
            $existingImages = $db->loadObjectList();

            foreach ($existingImages as $image) {
                $removedFiles[] = $mtconf->getjconf('absolute_path')
                    . $mtconf->get('relative_path_to_listing_small_image') . $image->filename;
                $removedFiles[] = $mtconf->getjconf('absolute_path')
                    . $mtconf->get('relative_path_to_listing_medium_image') . $image->filename;
                $removedFiles[] = $mtconf->getjconf('absolute_path')
                    . $mtconf->get('relative_path_to_listing_original_image') . $image->filename;
            }

            $query = $db->getQuery(true);
            $query->delete($db->quoteName('#__mt_images'));
            $query->where(array('link_id = ' . $linkId));
            $db->setQuery($query);
            $db->execute();
        }

        JLoader::import('joomla.filesystem.file');
        JFile::delete($removedFiles);

        if (isset($file)) {
            $image_dimension = getimagesize($file['tmp_name']);

            $valid = true;
            if ($mtconf->get('image_maxsize') > 0 && $file['size'] > $mtconf->get('image_maxsize')) {
                $valid = false;
            } elseif ($mtconf->get('image_min_width') > 0 && $image_dimension[0] < $mtconf->get('image_min_width')) {
                $valid = false;
            } elseif ($mtconf->get('image_min_height') > 0 && $image_dimension[0] < $mtconf->get('image_min_height')) {
                $valid = false;
            } elseif (empty($file['name']) || $file['error'] > 0) {
                $file_extension = pathinfo($file['name']);
                $file_extension = strtolower($file_extension['extension']);
                if (!in_array($file_extension, array('png', 'gif', 'jpg', 'jpeg'))) {
                    $valid = false;
                }
            }

            if ($valid) {
                $now = time();
                $fileName = $now . '-' . $file['name'];

                $mtImage = new mtImage();
                $mtImage->setMethod($mtconf->get('resize_method'));
                $mtImage->setQuality($mtconf->get('resize_quality'));
                $mtImage->setSize($mtconf->get('resize_small_listing_size'));
                $mtImage->setTmpFile($file['tmp_name']);
                $mtImage->setType($file['type']);
                $mtImage->setName($fileName);
                $mtImage->setSquare($mtconf->get('squared_thumbnail'));

                if (!$mtImage->resize()) {
                    return;
                }

                $mtImage->setDirectory($mtconf->getjconf('absolute_path') . $mtconf->get('relative_path_to_listing_small_image'));
                $mtImage->saveToDirectory();

                $mtImage->setSize($mtconf->get('resize_medium_listing_size'));
                $mtImage->setSquare(false);
                $mtImage->resize();
                $mtImage->setDirectory($mtconf->getjconf('absolute_path') . $mtconf->get('relative_path_to_listing_medium_image'));
                $mtImage->saveToDirectory();

                move_uploaded_file($file['tmp_name'], $mtconf->getjconf('absolute_path') . $mtconf->get('relative_path_to_listing_original_image') . $fileName);

                $imgObj = new stdClass();
                $imgObj->link_id = $row->link_id;
                $imgObj->filename = $fileName;
                $imgObj->ordering = 1;
                $db->insertObject('#__mt_images', $imgObj);
            }
        }

        $app = JFactory::getApplication();
        if ($row->link_approved) {
            $app->redirect(JRoute::_('index.php?option=com_mtree&task=viewlink&link_id=' . $row->link_id));
        } else {
            $app->redirect(JRoute::_( 'index.php?option=com_mtree&task=viewowner&user_id=' . $row->user_id));
        }
    }
}