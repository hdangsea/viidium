<div class="vi-editing-page">
    <form id="frmSaveListing" action="" method="post" enctype="multipart/form-data">
        <input type="hidden" name="option" value="com_viidium" />
        <input name="task" type="hidden" value="listing.save" />
        <input name="link_id" type="hidden" value="<?php echo $this->link_id?>" />
        <input name="link_name" type="hidden" value="" />
        <input name="link_desc" type="hidden" value="" />
        <div class="vi-controls-bar">
            <div class="vi-editing-category">
                <select name="cat_id" data-category>
                    <?php foreach ($this->cats as $cat) : ?>
                        <option value="<?php echo $cat['cat_id']?>"><?php echo $cat['cat_name']?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="vi-controls uk-float-right">
                <button data-publish-listing class="uk-button" data-link-id="0" type="submit" name="published" value="1">
                    <?php echo JText::_('COM_VIIDIUM_PUBLISH')?>
                </button>
                <button data-save-as-draft class="uk-button" type="submit" name="approve" value="0">
                    <?php echo JText::_('COM_VIIDIUM_SAVE_AS_DRAFT')?>
                </button>
            </div>
        </div>

        <div class="vi-editable"></div>

        <div id="vi-waiting-dlg" class="uk-modal">
            <div class="uk-modal-dialog">
                <div class="uk-modal-spinner"></div>
                <div class="uk-text-center vi-msg"><?php echo JText::_('COM_VIIDIUM_WAITING_FOR_SAVING')?></div>
            </div>
        </div>
    </form>
</div>