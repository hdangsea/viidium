<?php
class ViidiumViewSavelisting extends JViewLegacy {
    public function display($tpl = null) {
        global $mtconf;

        JHtml::_('jquery.framework');

        $this->cats = null;
        $viidiumParams = JComponentHelper::getParams('com_viidium');
        $listingCat = $viidiumParams->get('listing_category', 0);

        getCatsSelectlist($listingCat, $this->cats, 1);

        $document = JFactory::getDocument();

        $document->addScript('media/com_viidium/js/editor/jquery.ui.widget.js');
        $document->addScript('media/com_viidium/js/editor/jquery.iframe-transport.js');
        $document->addScript('media/com_viidium/js/editor/jquery.fileupload.js');
        $document->addScript('media/com_viidium/js/editor/jquery.cycle.all.js');
        $document->addScript('media/com_viidium/js/editor/outerHTML-2.1.0.js');
        $document->addScript('media/com_viidium/js/editor/util.js');
        $document->addScript('media/com_viidium/js/editor/medium-editor.js');
        $document->addScript('media/com_viidium/js/editor/handlebars-v3.0.0.js');
        $document->addScript('media/com_viidium/js/editor/jquery-sortable-min.js');
        $document->addScript('media/com_viidium/js/editor/templates.js');
        $document->addScript('media/com_viidium/js/editor/exttemplates.js');
        $document->addScript('media/com_viidium/js/editor/core.js');
        $document->addScript('media/com_viidium/js/editor/embeds.js');
        $document->addScript('media/com_viidium/js/editor/images.js');
        $document->addScript('media/com_viidium/js/editor/extimages.js');
        $document->addScript('media/com_viidium/js/editor/coverimage.js');
        $document->addScript('media/com_viidium/js/editor/script.js');
        $document->addScript('media/com_viidium/js/script.js');

        $document->addStyleSheet('//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css');
        $document->addStyleSheet('media/com_viidium/css/editor/medium-editor.min.css');
        $document->addStyleSheet('media/com_viidium/css/editor/default.min.css');
        $document->addStyleSheet('media/com_viidium/css/editor/medium-editor-insert-plugin.css');

        $saveUrl = JRoute::_('index.php?option=com_viidium&task=listing.save');
        $document->addScriptDeclaration("jQuery(document).ready(function() {Viidium.saveListingURL = '{$saveUrl}'});");

        $this->link_id = $link_id = JFactory::getApplication()->input->getInt('link_id');
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select(array('l.*', 'i.img_id', 'i.filename'));
        $query->from($db->quoteName('#__mt_links', 'l'));
        $query->leftJoin($db->quoteName('#__mt_images', 'i') . ' ON i.link_id = l.link_id');
        $query->where('l.link_id = ' . $db->quote($link_id));
        $db->setQuery($query);

        $this->link = $db->loadObject();
        $user = JFactory::getUser();

        # Check permission
        if (($mtconf->get('user_addlisting') == 1 && $user->id < 1) || ($link_id > 0 && $user->id == 0)){
            JFactory::getApplication('site')->redirect(
                JRoute::_(
                    'index.php?option=com_users&view=login&return='
                    . base64_encode(JRoute::_('index.php?option=com_viidium&view=savelisting' . (($link_id > 0)?'&link_id='.$link_id:'')))
                ),
                JText::sprintf('COM_MTREE_PLEASE_LOGIN_BEFORE_ADDLISTING')
            );
        } elseif (($link_id > 0 && $user->id <> $this->link->user_id)
            || ($mtconf->get('user_allowmodify') == 0 && $link_id > 0)
            || ($mtconf->get('user_addlisting') == -1 && $link_id == 0)
            || ($mtconf->get('user_addlisting') == 1 && $user->id == 0)) {

            return JError::raiseError(404,JText::_('JGLOBAL_RESOURCE_NOT_FOUND'));
        }

        $postContent = '';
        $postIntroduction = '';
        $postTitle = '';
        $postCoverPhotoUrl = '';
        if ($this->link) {
            require_once JPATH_COMPONENT . '/libs/simple_html_dom.php';
            $html = str_get_html($this->link->link_desc);

            $nodeContentData = $html->find('[id=vi-post-content]');
            $nodeIntroductionData = $html->find('[id=vi-post-introduction]');
            $nodeTitleData = $html->find('[id=vi-post-title]');

            $nodeContent = !empty($nodeContentData)?$nodeContentData[0]:'';
            $nodeIntroduction = !empty($nodeIntroductionData)?$nodeIntroductionData[0]:'';
            $nodeTitle = !empty($nodeTitleData)?$nodeTitleData[0]:'';

            $postContent = !empty($nodeContent)?addslashes(trim($nodeContent->outertext)):'';
            $postIntroduction = !empty($nodeIntroduction)?addslashes(trim($nodeIntroduction->outertext)):'';
            $postTitle = !empty($nodeTitle)?addslashes(trim($nodeTitle->outertext)):'';

            if (!empty($this->link->img_id)) {
                $postCoverPhotoUrl = $mtconf->getjconf('live_site')
                    . $mtconf->get('relative_path_to_listing_original_image') . $this->link->filename;
            }
        }

        $token = JSession::getFormToken();
        $imageUploadUrl = JRoute::_('index.php?option=com_viidium&task=file.upload&' . $token . '=1');
        $imageDeleteUrl = JRoute::_('index.php?option=com_viidium&task=file.delete&' . $token . '=1');

        $document->addScriptDeclaration("
            jQuery(document).ready(function() {
                Viidium.postContent = '$postContent';
                Viidium.postIntroduction = '$postIntroduction';
                Viidium.postTitle = '$postTitle';
                Viidium.uploadImageUrl = '$imageUploadUrl';
                Viidium.deleteImageUrl = '$imageDeleteUrl';
                Viidium.coverPhotoUrl = '$postCoverPhotoUrl';
                Viidium.token = '$token';

                Viidium.initEditingPage();
            });
        ");

        parent::display($tpl);
    }
}