<?php

defined('_JEXEC') or die;
jimport('joomla.html.pagination');

class ViidiumViewArchives extends JViewLegacy {
    public function display($tpl = null) {
        $mainframe = JFactory::getApplication();
        $input = JFactory::getApplication()->input;
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $this->recent = $input->get("recent", "");
        $offset = $input->get("start", "0");
        $user = $input->get("user", "");
        $limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
        if ($this->recent != "") {
            $this->recent = date("Y-n", strtotime($this->recent));
            $days = date("t", strtotime($this->recent));
            $query->select($db->quoteName(array('l.link_id', 'l.link_name', 'im.filename')));
            $query->from($db->quoteName('#__mt_links', 'l'));
            $query->join('INNER', $db->quoteName('#__mt_images', 'im') . ' ON (' . $db->quoteName('l.link_id') . ' = ' . $db->quoteName('im.link_id') . ')');
            $conditions = array(
                $db->quoteName('l.link_published') . ' = 1 ',
                $db->quoteName('l.link_approved') . ' = 1 ',
                'DATE(l.publish_up) between \'' . $this->recent . "-1" . '\' and \'' . $this->recent . '-' . $days . '\''
            );
            if ($user != "") {
                array_push($conditions, $db->quoteName('l.user_id') . ' = ' . $user);
            }
            array_push($conditions, $db->quoteName('l.publish_down') . ' = \'0000-00-00 00:00:00\' OR DATE(l.publish_down) >= NOW()');
            $query->where($conditions);
            $query->order('l.publish_up DESC');
            $db->setQuery($query, $offset, $limit);
            $this->listings = $db->loadObjectList();

            $query->clear('select');
            $query->clear('limit');
            $query->select('count(*) as Total');
            $db->setQuery($query);
            $this->total = $db->loadObjectList();

            $this->pagination = new JPagination($this->total[0]->Total, $offset, $limit);
        } else {
            $this->from = $input->get("from", "");
            $this->to = $input->get("to", "");
            if ($this->from != "" || $this->to != "") {
                if ($this->from == "")
                    $this->from = date("Y-n", strtotime($this->to)) . '-1';
                else
                    $this->from = date("Y-n-d", strtotime($this->from));
                if ($this->to == "")
                    $this->to = date("Y-n-d");
                else
                    $this->to = date("Y-n-d", strtotime($this->to));
                $query->select($db->quoteName(array('l.link_id', 'l.link_name', 'im.filename')));
                $query->from($db->quoteName('#__mt_links', 'l'));
                $query->join('INNER', $db->quoteName('#__mt_images', 'im') . ' ON (' . $db->quoteName('l.link_id') . ' = ' . $db->quoteName('im.link_id') . ')');
                $conditions = array(
                    $db->quoteName('l.link_published') . ' = 1 ',
                    $db->quoteName('l.link_approved') . ' = 1 ',
                    'DATE(l.publish_up) between \'' . $this->from . '\' and \'' . $this->to . '\''
                );
                if ($user != "") {
                    array_push($conditions, $db->quoteName('l.user_id') . ' = ' . $user);
                }
                array_push($conditions, 'l.publish_down = \'0000-00-00 00:00:00\' OR DATE(l.publish_down) >= NOW()');
                $query->where($conditions);
                $query->order('l.publish_up DESC');
                $db->setQuery($query, $offset, $limit);
                $this->listings = $db->loadObjectList();

                $query->clear('select');
                $query->clear('limit');
                $query->select('count(*) as Total');
                $db->setQuery($query);
                $this->total = $db->loadObjectList();

                $this->pagination = new JPagination($this->total[0]->Total, $offset, $limit);
            }
        }

        parent::display($tpl);
    }
}
