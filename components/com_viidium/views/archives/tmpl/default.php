<?php
defined('_JEXEC') or die('Restricted Access');
JHtml::_('behavior.tooltip');
?>
<?php global $mtconf; ?>
<div id="index" class="vi-content-layout">
    <h2
        class="title vi-category-title vi-category-title-archives"><?php echo JText::_("COM_VIIDIUM_ARCHIVES_PAGE") ?></h2>
    <?php
    echo '<div class="uk-grid vi-grid" data-uk-grid-match="{target:\'.vi-sub-category-title > div\'}" data-uk-grid-margin="">';
    foreach ($this->listings as $listing):
        echo '<div class="uk-width-medium-1-4 uk-width-small-1-2"><div class="vi-sub-category uk-text-center">';
        echo '<a class="vi-sub-category-image" href="' . JRoute::_("index.php?option=com_mtree&task=viewlink&link_id=" . $listing->link_id) . '">';
        if (isset($listing->filename) && $listing->filename <> '') {
            echo '<div style="background-image: url(\'' . $mtconf->getjconf('live_site') . $mtconf->get('relative_path_to_listing_medium_image') . $listing->filename . '\')" alt="' . htmlspecialchars($listing->link_name) . '"></div>';
        } else {
            echo '<div style="background-image: url(\'' . $mtconf->getjconf('live_site') . $mtconf->get('relative_path_to_listing_medium_image') . 'noimage_thb.png' . '\')" alt="' . htmlspecialchars($listing->link_name) . '" ></div>';
        }
        echo '</a>';
        ?>
        <h3 class="vi-sub-category-title">
            <div>
                <a href="<?php echo JRoute::_('index.php?option=com_mtree&task=viewlink&link_id=' . $listing->link_id); ?>"><?php echo $listing->link_name; ?></a>
            </div>
        </h3>
        <?php echo "</div></div>"; ?>
    <?php endforeach; ?>
    <?php echo '</div>'; ?>
    <div class="pagination">
        <p class="counter">
            <?php echo $this->pagination->getPagesCounter(); ?>
        </p>
        <?php echo $this->pagination->getPagesLinks(); ?>
    </div>
</div>