<ul class="vi-new-listings-view vi-mt-listings-list uk-list" data-limit="<?php echo $this->limit?>" data-cat-id="<?php echo $this->catId?>"
    data-fetch-more-items-url="<?php echo JRoute::_('index.php?option=com_viidium&view=listing&format=json')?>"
    data-max-difference="<?php echo $this->maxDifference?>">
    <?php
        global $mtconf;

        require_once JPATH_BASE . '/components/com_viidium/libs/Parsedown.php';
        $this->parseDown = new Parsedown();

        $this->mtconf = $mtconf;

    ?>
    <?php if (is_array($this->listings)) :?>
        <?php foreach ($this->listings as $idx => $l) : ?>
            <?php
                $this->user = JFactory::getUser($l->user_id);
                $desField = $this->fields->getFieldById(2);
                $this->maxChars = $desField->getParam('summaryChars');
                $this->stripSummaryTags = $desField->getParam('stripSummaryTags', 1);
            ?>
            <?php if (!$l->link_featured) : ?>
                <?php
                    $this->listing = $l;
                    echo $this->loadTemplate('listing')
                ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
</ul>