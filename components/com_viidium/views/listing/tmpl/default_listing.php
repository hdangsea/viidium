<?php $l = $this->listing;?>
<li class="" data-link-id="<?php echo $l->link_id?>">
    <div class="uk-grid uk-position-relative">
        <div class="uk-width-2-5">
            <?php
            if(isset($l->image_path) && !empty($l->image_path)) {
                $img = $l->image_path;
            } else {
                $img = $this->mtconf->getjconf('live_site')
                    . $this->mtconf->get('relative_path_to_images') . 'noimage_thb.png';
            }
            ?>
            <div class="vi-listing-image" style="background-image: url('<?php echo $img?>')"></div>
        </div>
        <div class="uk-width-3-5">
            <h3>
                <a class="vi-listing-title" href="<?php echo JRoute::_( 'index.php?option=com_mtree&task=viewlink&link_id=' . $l->link_id);?>">
                    <?php echo $l->link_name?>
                </a>
            </h3>

            <div class="vi-listing-des">
                <?php
                $desc = $this->parseDown->text($l->link_desc);
                $desc = trim(JString::substr($desc, 0, $this->maxChars));
                if ($this->stripSummaryTags) {
                    echo strip_tags($desc);
                } else {
                    echo $desc;
                }

                $userUrl = JRoute::_( 'index.php?option=com_mtree&task=viewowner&user_id=' . $this->user->id);
                $groups = JAccess::getGroupsByUser($this->user->id, false);
                ?>
            </div>

            <div class="vi-profile-info">
                <div class="uk-display-inline-block uk-text-right vi-info">
                    <div class="uk-text-muted">
                        <a href="<?php echo $userUrl?>" title="<?php echo $this->user->name?>"><?php echo $this->user->name?></a>
                    </div>
                    <div class="uk-text-muted vi-user-group">
                        <?php foreach ($groups as $gId) : ?>
                            <?php
                                if (isset($this->allGroups)) {
                                    $group = $this->allGroups[$gId];
                                    echo '<span data-group="' . $gId . '">' . $group->title . '</span>';
                                } else {
                                    echo '<span data-group="' . $gId . '"></span>';
                                }
                            ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="uk-display-inline-block">
                    <?php
                    // Show Owner Profile Picture
                    $profilepictureEnabled = JPluginHelper::isEnabled('user', 'profilepicture');
                    ?>
                    <?php if ($profilepictureEnabled) : ?>
                        <a href="<?php echo $userUrl?>" title="<?php echo $this->user->name?>">
                            <?php
                            jimport('mosets.profilepicture.profilepicture');
                            $profilepicture = new ProfilePicture($l->user_id);
                            if ($profilepicture->exists()) {
                                echo '<img src="' . $profilepicture->getURL(PROFILEPICTURE_SIZE_200) . '" alt="' . $this->user->name . '" />';
                            } else {
                                echo '<img src="' . $profilepicture->getFillerURL(PROFILEPICTURE_SIZE_200) . '" alt="' . $this->user->name . '" />';
                            }
                            ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</li>