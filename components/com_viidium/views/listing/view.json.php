<?php

class ViidiumViewListing extends JViewLegacy {
    const DEFAULT_LIMIT = 5;

    public function display($tpl = null) {
        global $mtconf;

        $document = JFactory::getDocument();
        $input = JFactory::getApplication()->input;

        if(!class_exists('ViidiumListingHelper')) {
            JLoader::import('helpers.listing', JPATH_COMPONENT);
        }

        $page = $input->getInt('page', 0);
        $limit = $input->getInt('limit', self::DEFAULT_LIMIT);
        $listings = ViidiumListingHelper::getListings($input->getInt('catId', 0), $page * $limit, $limit);

        $cf_ids = getAssignedFieldsID(0);

        # Load all CORE and custom fields
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->from($db->quoteName('#__mt_customfields', 'cf'));
        $query->select(array('cf.*'));
        $db->setQuery( "SELECT cf.*, '0' AS link_id, '' AS value, '0' AS attachment, '". '0' ."' AS cat_id FROM #__mt_customfields AS cf "
            .	"\nWHERE cf.published='1' && (filter_search = '1' || advanced_search = '1')"
            .	((!empty($cf_ids))?"\nAND cf.cf_id IN (" . implode(',',$cf_ids). ") ":'')
            .	" ORDER BY ordering ASC"
        );
        $fields = new mFields($db->loadObjectList());

        $htmlListings = array();

        $this->mtconf = $mtconf;
        require_once JPATH_BASE . '/components/com_viidium/libs/Parsedown.php';
        $this->parseDown = new Parsedown();

        $desField = $fields->getFieldById(2);
        $this->maxChars = $desField->getParam('summaryChars');
        $this->stripSummaryTags = $desField->getParam('stripSummaryTags', 1);
        foreach ($listings as $listing) {
            $this->listing = $listing;
            $this->user = JFactory::getUser($listing->user_id);
            $htmlListings[$listing->link_id] = $this->loadTemplate('listing');
        }

        $response = new JObject();
        $response->listings = $htmlListings;
        $json = json_encode($response);
        $callback = JRequest::getCmd('callback');
        if ($callback) {
            $document->setMimeEncoding('application/javascript');
            echo $callback .'('.$json.')';
        } else {
            echo $json;
        }
    }
}