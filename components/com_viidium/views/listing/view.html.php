<?php

class ViidiumViewListing extends JViewLegacy {
    public function display($tpl = null) {
        JHtml::_('jquery.framework');
        $document = JFactory::getDocument();
        $document->addScript('media/com_viidium/js/script.js');

        $menu = JFactory::getApplication()->getMenu();
        $active = $menu->getActive();
        if (!empty($active)) {
            $this->params = $active->params;
        } else {
            $this->params = new Registry();
        }

        if(!class_exists('ViidiumListingHelper')) {
            JLoader::import('helpers.listing', JPATH_COMPONENT);
        }

        $this->catId = $this->params->get('catId', 0);
        $initLimit = $this->params->get('initLimit', 0);
        $this->maxDifference = $this->params->get('maxDifference', 1000);
        $this->listings = ViidiumListingHelper::getListings($this->catId, 0, $initLimit);

        $cf_ids = getAssignedFieldsID(0);

        # Load all CORE and custom fields
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->from($db->quoteName('#__mt_customfields', 'cf'));
        $query->select(array('cf.*'));
        $db->setQuery( "SELECT cf.*, '0' AS link_id, '' AS value, '0' AS attachment, '". '0' ."' AS cat_id FROM #__mt_customfields AS cf "
            .	"\nWHERE cf.published='1' && (filter_search = '1' || advanced_search = '1')"
            .	((!empty($cf_ids))?"\nAND cf.cf_id IN (" . implode(',',$cf_ids). ") ":'')
            .	" ORDER BY ordering ASC"
        );
        $this->fields = new mFields($db->loadObjectList());

        $query->clear();
        $query->select('*');
        $query->from($db->quoteName('#__usergroups'));
        $db->setQuery($query);
        $this->allGroups = $db->loadObjectList('id');
        $document->addScriptDeclaration("
            jQuery(document).ready(function() {
                Viidium.userGroups = " . json_encode($this->allGroups) . ";" .
                "Viidium.setContainerElement('.vi-new-listings-view');" . "
            });
        ");

        $this->limit = $this->params->get('limit', 0);

        parent::display($tpl);
    }
}