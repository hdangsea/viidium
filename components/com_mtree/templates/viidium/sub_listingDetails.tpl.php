<?php
    global $Itemid;
    $user = JFactory::getUser($this->link->user_id);

    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select(array('cat.cat_id', 'cat.cat_name', 'cat.alias'));
    $query->from($db->quoteName('#__mt_cats', 'cat'));
    $query->innerJoin($db->quoteName('#__mt_cl', 'cl') . ' ON cat.cat_id = cl.cat_id');
    $query->where('cl.link_id = ' . $this->link_id);
    $query->order('cl.main DESC');
    $db->setQuery($query);
    $cats = $db->loadObjectList('cat_id');
?>
<div class="link-id-<?php echo $this->link_id; ?> cat-id-<?php echo $this->link->cat_id; ?> tlcat-id-<?php echo $this->link->tlcat_id; ?> uk-grid-preserve"
     id="listing" itemscope itemtype="http://schema.org/Thing">
    <div class="vi-detail-post uk-position-relative">
        <?php if ($this->link->link_image) : ?>
            <img id="vi-image-cover-bg" title="<?php echo $this->link->link_name?>"
                 src="<?php echo $this->jconf['live_site'] . $this->mtconf['relative_path_to_listing_original_image'] . $this->link->link_image;?>"  />
        <?php endif; ?>

        <div class="vi-detail-post-content">
            <?php echo $this->link->link_desc;?>
        </div>
        <div class="tm-post-info vi-control-post-info">
            <div class="vi-post-info-right">
                <?php if ($this->my->id == $this->link->user_id &&
                    ($this->config->get('user_allowmodify') == 1 || $this->config->get('user_allowdelete') == 1)
                    && $this->my->id > 0) : ?>
                    <div class="btn-group pull-right"> <a class="btn dropdown-toggle" data-toggle="dropdown" href="#" role="button"> <span class="icon-cog"></span> <span class="caret"></span> </a>
                        <ul class="dropdown-menu">
                            <?php if( $this->config->get('user_allowmodify') == 1) : ?>
                                <li class="edit-icon">
                                    <a href="<?php echo JRoute::_('index.php?option=com_viidium&view=savelisting&link_id='.$this->link->link_id); ?>">
                                        <span class="icon-edit"></span>
                                        <?php echo JText::_( 'COM_MTREE_EDIT' ); ?>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <?php if( $this->link->link_published && $this->link->link_approved && $this->config->get('user_allowdelete') == 1) : ?>
                                <li class="delete-icon">
                                    <a href="<?php echo JRoute::_('index.php?option=com_mtree&task=deletelisting&link_id='.$this->link->link_id); ?>">
                                        <span class="icon-delete"></span>
                                        <?php echo JText::_( 'COM_MTREE_DELETE' ); ?>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
            <div class="uk-clearfix"></div>
        </div>
    </div>

    <div class="uk-panel uk-panel-space">
        <?php
            if ($this->mtconf['use_map']) {
                include $this->loadTemplate('sub_map.tpl.php');
            }

            if ($this->mt_show_review) {
                include $this->loadTemplate('sub_reviews.tpl.php');
            }

            if (isset($this->links)) {
                include $this->loadTemplate('sub_listings.tpl.php');
            }
        ?>

        <?php
            $modCatListing = JModuleHelper::getModule('mod_mt_cat_listings');
            if ($modCatListing->id) {
                if ($modCatListing->showtitle == '1') {
                    echo '<h3 class="uk-panel-title">' . $modCatListing->title . '</h3>';
                }
                echo JModuleHelper::renderModule($modCatListing);
            }

            $modListings = JModuleHelper::getModule('mod_mt_listings');
            if ($modListings->id) {
                if ($modListings->showtitle == '1') {
                    echo '<h3 class="uk-panel-title">' . $modListings->title . '</h3>';
                }
                echo JModuleHelper::renderModule($modListings);
            }
        ?>
    </div>
</div>

<div class="vi-footer-bar uk-panel">
    <div class="vi-post-info uk-float-left">
        <?php
            $userImageUrl = $this->profilepicture_url;
            if (empty($userImageUrl)) {
                $userImageUrl = $this->jconf['live_site'] . '/media/plg_user_profilepicture/images/filler/200.png';
            }
        ?>
        <?php
            $authorUrl = JRoute::_( 'index.php?option=com_mtree&task=viewowner&user_id=' . $this->link->user_id);
            $authorLink = '<a href="' . $authorUrl . '">' . $user->name . '</a>';

            $userImgLink = "<a href='$authorUrl'><img src='$userImageUrl' /></a>";

            $arrCatLinks = array();
            foreach ($cats as $cat) {
                $link = JRoute::_('index.php?option=com_mtree&view=listcats&cat_id=' . $cat->cat_id);
                $arrCatLinks[] = '<a href="' . $link . '">' . $cat->cat_name . '</a>';
            }

            $createdDate = JFactory::getDate($this->link->link_created);

            echo sprintf(JText::_('COM_MTREE_BY_AUTHOR_ON'),
                $userImgLink, $authorLink, implode(', ', $arrCatLinks), $createdDate->format('M d Y'));
        ?>
    </div>

    <div class="uk-float-right">
        <div class="vi-controls">
            <?php if($this->config->get('show_favourite') == 1 || $this->config->get('show_rating') == 1): ?>
                <?php if($this->config->get('show_favourite')) : ?>
                    <span class="favourite">
                        <span class="fav-caption"><?php echo JText::_('COM_MTREE_FAVOURED')?>:</span>
                        <div id="fav-count"><?php echo number_format($this->total_favourites,0,'.',',') ?></div>
                        <?php if($this->my->id > 0) : ?>
                            <?php if($this->is_user_favourite) : ?>
                                <div id="fav-msg">
                                    <a href="javascript:fav(<?php echo $this->link->link_id ?>,-1);">
                                        <?php echo JText::_('COM_MTREE_REMOVE_FAVOURITE')?>
                                    </a>
                                </div>
                            <?php else : ?>
                                <div id="fav-msg">
                                    <a href="javascript:fav(<?php echo $this->link->link_id ?>,1);">
                                        <?php echo JText::_('COM_MTREE_ADD_AS_FAVOURITE')?>
                                    </a>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </span>
                <?php endif; ?>

                <span class="tm-rating">
                    <?php
                    echo $this->plugin('ratableRating',
                        $this->link, $this->link->link_rating, $this->link->link_votes);
                    ?>
                </span>
                <span id="total-votes">
                    <?php
                    if( $this->link->link_votes <= 1 ) {
                        echo $this->link->link_votes . " " . strtolower(JText::_( 'COM_MTREE_VOTE' ));
                    } elseif ($this->link->link_votes > 1 ) {
                        echo $this->link->link_votes . " " . strtolower(JText::_( 'COM_MTREE_VOTES' ));
                    }
                    ?>
                </span>
            <?php endif; ?>
            <span class="tm-reviews">
                <a href="#listing-reviews">
                    <?php echo JText::plural('COM_MTREE_NUM_REVIEWS', $this->total_reviews) ?>
                </a>
            </span>
            <a class="tm-post-link-action" title="<?php echo JText::_('COM_MTREE_RECOMMEND')?>"
               href="<?php echo JRoute::_( 'index.php?option=com_mtree&task=recommend&link_id=' . $this->link->link_id)?>">
                <i class="uk-icon-thumbs-o-up"></i>
            </a>
            <a class="tm-post-link-action" title="<?php echo JText::_('COM_MTREE_PRINT')?>"
               href="<?php echo JRoute::_( 'index.php?option=com_mtree&task=print&link_id=' . $this->link->link_id . '&tmpl=component&Itemid=' . $Itemid)?>"
               onclick="javascript:void window.open(this.href, \'win2\', \'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no\'); return false;">
                <i class="uk-icon-print"></i>
            </a>
            <a class="tm-post-link-action" title="<?php echo JText::_('COM_MTREE_REPORT')?>"
               href="<?php echo JRoute::_( 'index.php?option=com_mtree&task=report&link_id=' . $this->link->link_id)?>">
                <i class="uk-icon-send"></i>
            </a>
            <a class="tm-post-link-action" title="<?php echo JText::_('COM_MTREE_CLAIM')?>"
               href="<?php echo JRoute::_( 'index.php?option=com_mtree&task=claim&link_id=' . $this->link->link_id)?>">
                <i class="uk-icon-bolt"></i>
            </a>
        </div>
    </div>
</div>
