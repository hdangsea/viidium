<?php
    require_once JPATH_BASE . '/components/com_viidium/libs/simple_html_dom.php';

    if ($i > 1) {
        $number_of_ls_columns = 2;
        if ($link->link_image) {
            $imgUrl = $this->config->getjconf('live_site') . $this->config->get('relative_path_to_listing_medium_image') . $link->link_image;
        }
    } else {
        $number_of_ls_columns = 1;
        if ($link->link_image) {
            $imgUrl = $this->config->getjconf('live_site') . $this->config->get('relative_path_to_listing_original_image') . $link->link_image;
        }
    }
?>
<div class="uk-width-small-1-2 uk-width-medium-1-<?php echo $number_of_ls_columns ?>" data-link-id="<?php echo $link->link_id; ?>">
<div class="vi-listing-summary <?php echo ($number_of_ls_columns == 1) ? 'vi-listing-summary-big' : '' ?>
        <?php echo ($link->link_featured && $this->config->getTemParam('useFeaturedHighlight', '1')) ? ' vi-featured' : ''; ?>">
<?php if ($this->config->getTemParam('showImageInSummary', 1)) : ?>
    <?php if ($link->link_image): ?>
        <?php if ($link->link_published && $link->link_approved): ?>
            <a class="vi-listing-link"
            href="<?php echo JRoute::_('index.php?option=com_mtree&task=viewlink&link_id=' . $link->link_id . '&Itemid=' . $this->Itemid); ?>">
        <?php endif; ?>
        <div class="vi-listing-img"
             style="background-image:url('<?php echo $imgUrl ?>')">
        </div>
        <?php if ($link->link_published && $link->link_approved): ?>
            </a>
        <?php endif; ?>
    <?php elseif ($this->config->getTemParam('showFillerImage', 1)) : ?>
        <?php if ($link->link_published && $link->link_approved): ?>
            <a href="<?php echo JRoute::_('index.php?option=com_mtree&task=viewlink&link_id=' . $link->link_id . '&Itemid=' . $this->Itemid); ?>">
        <?php endif; ?>
        <div class="vi-listing-img"
             style="background-image:url('<?php echo $this->config->getjconf('live_site') . $this->config->get('relative_path_to_images'); ?>noimage_thb.png')">
        </div>
        <?php if ($link->link_published && $link->link_approved): ?>
            </a>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>

<div class="vi-listing-info">
<div class="vi-title">
    <h3 class="vi-title-listing">
        <?php
        $link_name = $link_fields->getFieldById(1);
        switch ($this->config->getTemParam('listingNameLink', '1')) {
            default:
            case 1:
                $this->plugin(
                    'ahreflisting',
                    $link, $link_name->getOutput(2),
                    '',
                    array('delete' => false, 'edit' => false, 'title' => $link_name)
                );
                break;
            case 4:
                if (!empty($link->website)) {
                    $this->plugin(
                        'ahreflisting',
                        $link,
                        $link_name->getOutput(2),
                        '',
                        array("edit" => false, "delete" => false, 'title' => $link_name),
                        1);
                } else {
                    $this->plugin(
                        'ahreflisting',
                        $link,
                        $link_name->getOutput(2),
                        '',
                        array("edit" => false, "delete" => false, 'title' => $link_name)
                    );
                }
                break;
            case 2:
                $this->plugin(
                    'ahreflisting',
                    $link,
                    $link_name->getOutput(2),
                    '',
                    array("edit" => false, "delete" => false, 'title' => $link_name),
                    1
                );
                break;
            case 3:
                $this->plugin(
                    'ahreflisting',
                    $link,
                    $link_name->getOutput(2),
                    'target="_blank"',
                    array("edit" => false, "delete" => false, 'title' => $link_name),
                    1
                );
                break;
            case 0:
                $this->plugin(
                    'ahreflisting',
                    $link,
                    $link_name->getOutput(2),
                    '',
                    array("edit" => false, "delete" => false, 'link' => false, 'title' => $link_name)
                );
                break;
        }
        ?>
    </h3>

    <div class="vi-listing-rating">
        <?php
        // Rating
        $this->plugin('rating', $link->link_rating, $link->link_votes, $link->attribs);

        if ($this->config->get('show_review')) {
            echo '<span class="reviews">';
            echo '<a href="' . JRoute::_('index.php?option=com_mtree&task=viewlink&link_id=' . $link->link_id . '&Itemid=' . $this->Itemid) . '">' . $this->reviews_count[$link->link_id]->total . ' ' . strtolower(JText::_('COM_MTREE_REVIEWS')) . '</a>';
            echo '</span>';
        }
        ?>
    </div>
</div>

<?php if ($this->my->id == $link->user_id
    && ($this->config->get('user_allowmodify') == 1 || $this->config->get('user_allowdelete') == 1) && $this->my->id > 0
) : ?>
    <div class="btn-group pull-right"><a class="btn dropdown-toggle" data-toggle="dropdown" href="#" role="button">
            <span class="icon-cog"></span> <span class="caret"></span> </a>
        <ul class="dropdown-menu">
            <?php if ($this->config->get('user_allowmodify') == 1) { ?>
                <li class="edit-icon">
                    <a href="<?php echo JRoute::_('index.php?option=com_viidium&view=savelisting&link_id=' . $link->link_id); ?>">
                        <span class="icon-edit"></span>
                        <?php echo JText::_('COM_MTREE_EDIT'); ?>
                    </a>
                </li>
            <?php
            }

            if ($link->link_published && $link->link_approved && $this->config->get('user_allowdelete') == 1) {
                ?>
                <li class="delete-icon">
                    <a href="<?php echo JRoute::_('index.php?option=com_mtree&task=deletelisting&link_id=' . $link->link_id); ?>">
                        <span class="icon-remove"></span>
                        <?php echo JText::_('COM_MTREE_DELETE'); ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php endif; ?>

<?php
// Address
if ($this->config->getTemParam('displayAddressInOneRow', '1')) {
    $address_parts = array();
    foreach (array(4, 5, 6, 7, 8) as $address_field_id) {
        $field = $link_fields->getFieldById($address_field_id);
        if (isset($field) && ($output = $field->getOutput(1))) {
            $address_parts[] = $output;
        }
    }
    if (!empty($address_parts)) {
        echo '<p class="address">' . implode(', ', $address_parts) . '</p>';
    }
}

// Website
$website = $link_fields->getFieldById(12);
if (!is_null($website) && $website->hasValue()) {
    echo '<p class="website">' . $website->getOutput(2) . '</p>';
}

// Listing's category
if ($this->task <> 'listcats' && $this->task <> '') {
    echo '<div class="category"><span>' . JText::_('COM_MTREE_CATEGORY') . ': </span>';
    $this->plugin('mtpath', $link->cat_id, '');
    echo '</div>';
}

// Other custom field
$link_fields->resetPointer();
echo '<div class="fields">';
$number_of_columns = $this->config->getTemParam('numOfColumnsInSummaryView', '1');
$field_count = 1;
while ($link_fields->hasNext()) {
    $field = $link_fields->getField();
    $value = $field->getOutput(2);
    $hasValue = $field->hasValue();
    if (((!$field->hasInputField() && !$field->isCore() && empty($value)) || (!empty($value) || $value == '0')) &&
        !in_array($field->getId(), array(1, 2, 12)) && (($this->config->getTemParam('displayAddressInOneRow', '1')
                && !in_array($field->getId(), array(4, 5, 6, 7, 8)) || $this->config->getTemParam('displayAddressInOneRow', '1') == 0) && $hasValue)
    ) {
        echo "\n\t\t\t" . '<div id="field_' . $field->getId() . '" class="fieldRow ' . $field->getFieldTypeClassName() . ' span' . round(12 / $number_of_columns) . (($number_of_columns == 1 || $field_count % $number_of_columns == 0) ? ' lastFieldRow' : '') . '">';

        if ($field->hasCaption()) {
            echo "\n\t\t\t\t" . '<span class="caption">' . $field->getCaption() . '</span>';
            echo '<span class="output">';
            // Special case to always output Price field's 'display prefix' and 'display suffix' text
            if ($field->getId() == 13 && $field->getValue() > 0) {
                echo $field->getDisplayPrefixText();
                echo $field->getOutput(2);
                echo $field->getDisplaySuffixText();
            } else {
                echo $field->getOutput(2);
            }
            echo '</span>';
        } else {
            echo "\n\t\t\t\t" . '<span class="output">' . $field->getOutput(2) . '</span>';
        }
        echo "\n\t\t\t" . '</div>';
        $field_count++;
    }
    $link_fields->next();
}
echo '</div>';

if ($this->config->getTemParam('showActionLinksInSummary', '0')) {
    echo '<div class="actions">';
    $this->plugin('ahrefreview', $link, array("rel" => "nofollow", "class" => "btn btn-small"));
    $this->plugin('ahrefrecommend', $link, array("rel" => "nofollow", "class" => "btn btn-small"));
    $this->plugin('ahrefprint', $link, array("rel" => "nofollow", "class" => "btn btn-small"));
    $this->plugin('ahrefcontact', $link, array("rel" => "nofollow", "class" => "btn btn-small"));
    $this->plugin('ahrefvisit', $link, JText::_('COM_MTREE_VISIT'), 1, array("rel" => "nofollow", "class" => "btn btn-small"));
    $this->plugin('ahrefreport', $link, array("rel" => "nofollow", "class" => "btn btn-small"));
    $this->plugin('ahrefclaim', $link, array("rel" => "nofollow", "class" => "btn btn-small"));
    $this->plugin('ahrefownerlisting', $link, array("class" => "btn btn-small"));
    $this->plugin('ahrefmap', $link, array("rel" => "nofollow", "class" => "btn btn-small"));
    echo '</div>';
}
?>

<div class="vi-date-author-info">
                <span class="date">
                    <?php
                    $createdDate = JFactory::getDate($link->link_created);
                    echo $createdDate->format('Y.m.d');
                    ?>
                </span>
                <span>
                    <a href="<?php echo JRoute::_('index.php?option=com_mtree&task=viewuserslisting&user_id=' . $link->user_id) ?>">
                        <?php echo $link->username ?>
                    </a>
                </span>
</div>

<?php
if (!is_null($link_fields->getFieldById(2)) || $link->link_image) {
    echo '<p>';
    $content = str_get_html($link->link_desc);
    $contentNode = $content->find('[id=vi-post-introduction]');
    $desc = '';
    if (count($contentNode) > 0) {
        $desc = $contentNode[0]->plaintext;
    }
    if ($desc == '' && !is_null($link_fields->getFieldById(2))) {
        $link_desc = $link_fields->getFieldById(2);
        $desc = $link_desc->getOutput(2);
    }

    echo $desc;
    echo '</p>';
}
?>
</div>
</div>
</div>