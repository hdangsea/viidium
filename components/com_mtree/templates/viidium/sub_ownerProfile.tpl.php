<div class="uk-grid tm-owner-profile uk-margin">
	<div class="uk-width-1-6">
        <div class="tm-profile-picture">
            <?php
                // Specify the profile fields to be displayed here
                // Full list of field names:
                // profile_address1, profile_address2, profile_city, profile_country, profile_postal_code, profile_phone, profile_website, profile_aboutme
                $profile_fields = array('profile_city', 'profile_country', 'profile_website', 'profile_aboutme');

                // Show Owner Profile Picture
                $profilepicture_enabled = JPluginHelper::isEnabled( 'user', 'profilepicture' );
                if ($profilepicture_enabled) {
                    jimport('mosets.profilepicture.profilepicture');
                    $profilepicture = new ProfilePicture($this->owner->id);
                    if ($profilepicture->exists()) {
                        echo '<img src="'.$profilepicture->getURL(PROFILEPICTURE_SIZE_200).'" alt="'.$this->owner->username.'" />';
                    } else {
                        echo '<img src="'.$profilepicture->getFillerURL(PROFILEPICTURE_SIZE_200).'" alt="'.$this->owner->username.'" />';
                    }
                }
            ?>
        </div>
	</div>
	<div class="uk-width-5-6">
        <div class="uk-grid">
            <div class="uk-width-1-2">
                <h2><?php echo $this->owner->name?></h2>
                <?php foreach ($profile_fields as $profile_field): ?>
                    <?php
                        if (!isset($this->user_profile_fields[$profile_field])){
                            continue;
                        }
                        $profile = $this->user_profile_fields[$profile_field];
                    ?>
                    <?php if ($profile->value) : ?>
                        <div class="row-fluid">
                            <div class="span2 <?php echo $profile_field?>"><?php echo $profile->label?></div>
                            <?php
                                $profile->text = htmlspecialchars($profile->value, ENT_COMPAT, 'UTF-8');
                            ?>
                            <div class="span10 <?php echo $profile_field?>">
                                <?php if ($profile->id == 'profile_website') : ?>
                                    <?php
                                        $v_http = substr($profile->profile_value, 0, 4);
                                    ?>
                                    <?php if ($v_http == 'http') : ?>
                                        <a href="<?php echo $profile->text?>"><?php echo $profile->text?></a>
                                    <?php else: ?>
                                        <a href="http://<?php echo $profile->text?>"><?php echo $profile->text?></a>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <?php echo $profile->text;?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
                <?php if ($this->owner->id == $this->my->id) : ?>
                    <?php
                        $language = JFactory::getLanguage();
                        $language->load('com_users', JPATH_SITE, null, true);
                    ?>
                    <a href="<?php echo JRoute::_('index.php?option=com_users&task=profile.edit&user_id=' . $this->my->id)?>">
                        <?php echo JText::_('COM_USERS_EDIT_PROFILE')?>
                    </a>
                <?php endif; ?>
            </div>
            <div class="uk-width-1-2">
                <?php
                    $modules = JModuleHelper::getModules('invite-box');
                    foreach ($modules as $mod) {
                        if ($mod->showtitle == '1') {
                            echo '<h2>' . $mod->title . '</h2>';
                        }
                        echo JModuleHelper::renderModule($mod);
                    }
                ?>
            </div>
        </div>
	</div>
</div>