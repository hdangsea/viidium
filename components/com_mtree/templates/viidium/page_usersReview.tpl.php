<h2 class="vi-user-page-title">
    <?php
    if( $this->my->id == $this->owner->id ) {
        echo JText::_('COM_MTREE_MY_PAGE');
    } else {
        echo $this->owner->username;
    }
    ?>
</h2>
<?php include $this->loadTemplate('sub_ownerProfile.tpl.php'); ?>

<nav class="uk-navbar">
    <ul class="uk-navbar-nav">
        <li class="">
            <a href="<?php echo JRoute::_("index.php?option=com_mtree&task=viewuserslisting&user_id=".$this->owner->id."&Itemid=$this->Itemid") ?>">
                <?php echo JText::_('COM_MTREE_LISTINGS')?> (<?php echo $this->total_links?>)
            </a>
        </li>

        <?php if($this->mtconf['show_review']) :?>
            <li class="uk-active">
                <a href="<?php echo JRoute::_("index.php?option=com_mtree&task=viewusersreview&user_id=".$this->owner->id."&Itemid=$this->Itemid") ?>">
                    <?php echo JText::_('COM_MTREE_REVIEWS') ?> (<?php echo $this->pageNav->total ?>)
                </a>
            </li>
        <?php endif; ?>

        <?php if($this->mtconf['show_favourite']):?>
            <li class="">
                <a href="<?php echo JRoute::_("index.php?option=com_mtree&task=viewusersfav&user_id=".$this->owner->id."&Itemid=$this->Itemid") ?>">
                    <?php echo JText::_( 'COM_MTREE_FAVOURITES' ) ?> (<?php echo $this->total_favourites ?>)
                </a>
            </li>
        <?php endif; ?>
    </ul>
</nav>

<div class="tm-reviews uk-panel">
    <?php if (is_array($this->reviews) && !empty($this->reviews)) : ?>
        <ul class="uk-list">
            <?php foreach ($this->reviews AS $review):?>
                <li class="tm-review">
                    <?php if ($review->link_image) : ?>
                        <div class="tm-thumbnail uk-float-left">
                            <a href="index.php?option=com_mtree&task=viewlink&link_id=' . $review->link_id . '&Itemid=' . $this->Itemid . '">
                                <?php $this->plugin( 'mt_image', $review->link_image, '3', $review->link_name );?>
                            </a>
                        </div>
                    <?php endif; ?>

                    <div class="tm-review-content uk-float-left">
                        <div class="review-listing">
                            <?php $this->plugin('ahref', array("path"=>"index.php?option=".$this->option."&task=viewlink&link_id=".$review->link_id."&Itemid=".$this->Itemid), $review->link_name); ?>
                        </div>
                        <div class="review-head">
                            <div class="review-title">
                                <?php if($review->rating > 0): ?>
                                    <div class="review-rating">
                                        <?php $this->plugin( 'review_rating', $review->rating ); ?>
                                    </div>
                                <?php endif; ?>

                                <?php $this->plugin('ahref', array("path"=>"index.php?option=".$this->option."&task=viewlink&link_id=".$review->link_id."&Itemid=".$this->Itemid,"fragment"=>"rev-".$review->rev_id), $review->rev_title,'id="rev-'.$review->rev_id.'"');?>
                            </div>

                            <div class="review-info">
                                <?php echo JText::_('COM_MTREE_REVIEWED_BY')?>
                                <span class="review-owner">
                                    <?php echo ( ($review->user_id) ? $review->username : $review->guest_name); ?>
                                </span>,
                                <?php echo date("F j, Y",strtotime($review->rev_date)) ?>
                            </div>

                            <div id="rhc<?php echo $review->rev_id?>" class="found-helpful" <?php echo ($review->vote_total==0)?'style="display:none"':''?>>
                                <span id="rh<?php echo $review->rev_id?>">
                                    <?php
                                    if ($review->vote_total > 0) {
                                        echo JText::sprintf('COM_MTREE_PEOPLE_FIND_THIS_REVIEW_HELPFUL',
                                            $review->vote_helpful, $review->vote_total);
                                    }
                                    ?>
                                </span>
                            </div>

                            <div class="tm-review-text">
                                <?php echo $review->rev_text;?>

                                <?php if (!empty($review->ownersreply_text) && $review->ownersreply_approved): ?>
                                    <div class="owners-reply">
                                        <span><?php echo JText::_( 'COM_MTREE_OWNERS_REPLY' )?></span>
                                        <p><?php echo $review->ownersreply_text?></p>;
                                    </div>
                                <?php endif; ?>
                            </div>
                    </div>
                    <div class="uk-clearfix"></div>
                </li>
            <?php endforeach ?>
        </ul>
        <?php if ($this->pageNav->total > $this->pageNav->limit) :?>
            <div class="pagination">
                <p class="counter pull-right">
                    <?php echo $this->pageNav->getPagesCounter(); ?>
                </p>
                <?php echo $this->pageNav->getPagesLinks(); ?>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <center>
            <?php
                if( $this->my->id == $this->owner->id ) {
                    echo JText::_( 'COM_MTREE_YOU_DO_NOT_HAVE_ANY_REVIEWS' );
                } else {
                    echo JText::_( 'COM_MTREE_THIS_USER_DO_NOT_HAVE_ANY_REVIEWS' );
                }
            ?>
        </center>
    <?php endif; ?>
</div>