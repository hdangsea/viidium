<?php
defined('_JEXEC') or die;

?>
<?php
$input = JFactory::getApplication()->input;
$recent = $input->get("recent");
$my_page = JFactory::getURI();
$pos=strpos($my_page, "/my-page");
$user="";
if($pos)
    $user='&user='.JFactory::getUser()->id;
?>
<ul class="uk-nav">
    <?php
    foreach ($list as $key => &$value) {
        if ($recent == $key)
            echo '<li class="uk-active"><a href="' . JRoute::_('index.php?option=com_viidium&view=archives&recent=' . $key.$user) . '">' . $value . '</a></li>';
        else
            echo '<li><a href="' . JRoute::_('index.php?option=com_viidium&view=archives&recent=' . $key.$user) . '">' . $value . '</a></li>';
    }
    ?>
    <li>
        <form class="uk-form uk-form-stacked" method="get"
              action="<?php echo JRoute::_('index.php?option=com_viidium') ?>">
            <div class="uk-form-row uk-form-controls uk-form-controls-text uk-text-right">
                <label for="form-h-mix1" class="vi-label"><?php echo JText::_("MOD_RECENT_ARCHIVES_DATE_FROM");?></label>
                <input id="date-from" name="from" type="text" class="vi-textbox" placeholder="<?php echo JText::_("MOD_RECENT_ARCHIVES_DATE_FORMAT");?>" />
            </div>
            <div class="uk-form-row uk-form-controls uk-form-controls-text uk-text-right">
                <label for="form-h-mix2" class="vi-label"><?php echo JText::_("MOD_RECENT_ARCHIVES_DATE_TO");?></label>
                <input id="date-to" name="to" type="text" class="vi-textbox" placeholder="<?php echo JText::_("MOD_RECENT_ARCHIVES_DATE_FORMAT");?>" />
            </div>
            <div class="uk-form-row uk-form-controls uk-form-controls-text uk-text-right">
                <input class="uk-button uk-button-primary vi-button" value="Search" type="submit"/>
            </div>
            <input type="hidden" name="view" value="archives" />
            <?php if($user!=""):?>
            <input type="hidden" name="user" value="<?php echo $user?>" />
            <?php endif;?>
        </form>
    </li>
</ul>
