<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_random_image
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_random_image
 *
 * @package     Joomla.Site
 * @subpackage  mod_random_image
 * @since       1.5
 */
class ModRecentArchivesHelper
{
    public static function getList(&$params)
    {
        $list = 0;
        $module = JModuleHelper::getModule('mod_recent_archives');
        $prs = new JRegistry($module->params);
        $NumberOfRencentMonth = (int)$prs['number_of_recent_month'];
        $recent_month=array();
        for($i=0;$i<$NumberOfRencentMonth;$i++)
            $recent_month[date("Y-n",strtotime(date('Y-m')." -".$i." month"))]=date("F Y",strtotime(date('Y-m')." -".$i." month"));
        return $recent_month;
    }

}
