<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
    $document = JFactory::getDocument();
    $document->addStyleSheet("media/mod_mt_listings/rightside.css", "text/css", "screen");
?>
<ul class="mod_mt_listings mod_mt_listings_rightside uk-grid">
    <?php
        global $mtconf;
        $i = 0;
    ?>
    <?php if (is_array($listings)) : ?>
        <?php foreach ($listings AS $l) : ?>
            <?php
                $user = JFactory::getUser($l->user_id);
            ?>
            <li class="uk-grid uk-grid-preserve uk-width-1-2">
                <div class="uk-width-2-5">
                    <?php
                    if (isset($l->image_path) && !empty($l->image_path)) {
                        $img = $mtconf->getjconf('live_site') . $mtconf->get('relative_path_to_listing_small_image') . $l->image_path;
                    } else {
                        $img = $mtconf->getjconf('live_site') . $mtconf->get('relative_path_to_images') . 'noimage_thb.png';
                    }
                    ?>
                    <div class="tm-listing-image uk-animation-scale" style="background-image: url('<?php echo $img ?>'); background-position: 50% 50%;"></div>
                </div>
                <div class="uk-width-3-5">
                    <h4 class="tm-listing-detail">
                        <a href="<?php echo JRoute::_('index.php?option=com_mtree&task=viewlink&link_id=' . $l->link_id); ?>"><?php echo $l->link_name; ?></a>
                    </h4>

                    <div class="uk-text-muted">
                        <span>
                            <?php
                                $createdDate = JFactory::getDate($l->link_created);
                                echo $createdDate->format('Y.m.d');
                            ?>
                        </span>
                        &nbsp;
                        <span>
                            <a href="<?php echo JRoute::_('index.php?option=com_mtree&task=viewowner&user_id=' . $l->user_id) ?>">
                                <?php echo $user->name ?>
                            </a>
                        </span>
                    </div>
                    <?php if ($l->link_featured) : ?>
                        <div class="tm-featured-box uk-display-inline-block">
                            <?php echo JText::_('MOD_MT_LISTINGS_FEATURED') ?>
                        </div>
                    <?php endif; ?>
                </div>
            </li>
        <?php endforeach; ?>
    <?php endif; ?>
</ul>