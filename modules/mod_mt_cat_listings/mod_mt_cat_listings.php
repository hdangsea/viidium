<?php
require(JPATH_ROOT . '/components/com_mtree/init.module.php');
require_once(JPATH_ROOT . '/administrator/components/com_mtree/admin.mtree.class.php');

if (!$moduleHelper->isListingPage()) {
    return;
}

$input = JFactory::getApplication()->input;

$db = JFactory::getDbo();
$query = $db->getQuery(true);
$query->select(array('l.*', 'cl.cat_id'));
$query->from($db->quoteName('#__mt_links', 'l'));
$query->innerJoin($db->quoteName('#__mt_cl', 'cl') . ' ON cl.link_id = l.link_id');
$query->where('l.link_id = ' . $input->getInt('link_id'));
$query->where('cl.main = 1');
$db->setQuery($query);
$listing = $db->loadObject();

$query->clear('where');
$query->clear('select');
$query->select(array('l.*', 'img.filename AS image_path'));
$query->leftJoin($db->quoteName('#__mt_images', 'img') . ' ON img.link_id = l.link_id AND img.ordering = 1');
$query->where('cl.cat_id = ' . $listing->cat_id);
$query->where('l.link_id != ' . $listing->link_id);
$query->order('l.link_created DESC');
$db->setQuery($query, 0, $params->get('limit'));
$listings = $db->loadObjectList();

if (!empty($listings)) {
    foreach ($listings as $l) {
        $l->link = JRoute::_('index.php?option=mtree&task=viewlink&link_id=' . $l->link_id);
    }

    require(JModuleHelper::getLayoutPath('mod_mt_cat_listings'));
}


