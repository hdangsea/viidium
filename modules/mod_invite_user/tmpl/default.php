<?php
$queryParams = http_build_query(array('option' => 'com_viidium', 'task' => 'invitation.invite','tmpl'=>'component', JSession::getFormToken()=>'1'));
$url = JRoute::_('index.php?' . $queryParams);
?>
<form id="invite-form" class="uk-form tm-invite-form" action="<?php echo $url;?>">
    <div>
        <input type="email" name="email" placeholder="<?php echo JText::_('MOD_INVITE_USER_INPUT_YOUR_FRIEND_EMAIL')?>" required=""/>
        <button class="uk-button"><?php echo JText::_('MOD_INVITE_USER_INVITE')?></button>
    </div>
    <div id="number-of-invitation" class="uk-text-small">
        <?php if(isset($results[0]))
        {
            echo sprintf(JText::_('MOD_INVITE_USER_INVITATION_REMAINING'), $results[0]->remaining_invitation);
        }
        ?>
    </div>
</form>