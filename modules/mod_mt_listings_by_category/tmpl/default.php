<?php
    require_once JPATH_BASE . '/components/com_viidium/libs/Parsedown.php';

    global $mtconf;
    $maxChars = $mtconf->getTemParam('summaryChars', 255);
    $stripSummaryTags = $mtconf->getTemParam('stripSummaryTags', 255);
?>
<ul class="vi-mt-listings-by-category">
    <?php $idx = 0;?>
    <?php foreach ($links as $link) : ?>
        <li>
            <div class="vi-mt-categories">
                <?php
                    $listingCats = array();
                    foreach ($linkMapCats[$link['link_id']] as $catId) {
                        if ($catId != $cat_id && isset($cats[$catId])) {
                            array_push($listingCats, $cats[$catId]);
                        }
                    }
                ?>
                <?php foreach ($listingCats as $cat) : ?>
                    <span>
                        <a href="<?php echo $cat->link?>"><?php echo $cat->cat_name?></a>
                    </span>
                <?php endforeach; ?>
            </div>
            <h3>
                <a href="<?php echo JRoute::_( 'index.php?option=com_mtree&task=viewlink&link_id=' . $link['link_id']);?>">
                    <?php echo $link['link_name']?>
                </a>
            </h3>
            <div class="vi-mt-created-info">
                <span>
                    <?php
                        echo JText::sprintf(
                            'MOD_MT_LISTINGS_BY_CATEGORY_CREATED_BY',
                            JHtml::date($link['link_created'], 'd M, Y'),
                            '<a href="' . JRoute::_( 'index.php?option=com_mtree&task=viewowner&user_id=' . $link['user_id']) . '">'
                                . $link['userfullname']
                            . '</a>'
                        );
                    ?>
                </span>
            </div>
            <div class="vi-mt-desc">
                <?php
                    $parseDown = new Parsedown();
                    $desc = $parseDown->text($link['link_desc']);
                    $desc = trim(JString::substr($desc, 0, $maxChars));
                    if ($stripSummaryTags) {
                        echo strip_tags($desc);
                    } else {
                        echo $desc;
                    }
                ?>
            </div>

            <?php
                $idx++;
                if ($idx < count($links)) {
                    echo '<hr />';
                }
            ?>
        </li>
    <?php endforeach; ?>
</ul>
