<?php
require (JPATH_ROOT . '/components/com_mtree/init.module.php');
require_once (JPATH_ROOT . '/administrator/components/com_mtree/admin.mtree.class.php');
require_once (JPATH_SITE . '/components/com_mtree/listlisting.php');

$cat_id = $params->get('cat_id');

$database	= JFactory::getDbo();
$jdate		= JFactory::getDate();
$now		= $jdate->toSql();
$nullDate	= $database->getNullDate();
$user = JFactory::getUser();

$query = $database->getQuery(true);
$query->select(array('l.*', 'u.name as userfullname', 'u.username as username'));
$query->from($database->quoteName('#__mt_links', 'l'));
$query->leftJoin($database->quoteName('#__mt_cl', 'cl') . ' ON cl.link_id = l.link_id');
$query->leftJoin($database->quoteName('#__users', 'u') . ' ON u.id = l.user_id');
$query->where('link_published = 1');
$query->where('link_approved = 1');
$query->where('cl.cat_id = ' . $database->quote($cat_id));
$query->where("(publish_up = " . $database->quote($nullDate) . " OR publish_up <= '$now')");
$query->where("(publish_down = " . $database->quote($nullDate) . " OR publish_down >= '$now')");
$database->setQuery($query);
$links = $database->loadAssocList('link_id');
$linkIds = array_keys($links);

$query->clear();
$query->select('*');
$query->from($database->quoteName('#__mt_cl'));
$query->where('link_id IN (' . implode(',', $linkIds) . ')');
$database->setQuery($query);
$cls = $database->loadAssocList();

$linkMapCats = array();
$catIds = array();
foreach ($cls as $row) {
    if (!isset($linkMapCats[$row['link_id']])) {
        $linkMapCats[$row['link_id']] = array();
    }
    array_push($linkMapCats[$row['link_id']], $row['cat_id']);
    if (!in_array($row['cat_id'], $catIds)) {
        array_push($catIds, $row['cat_id']);
    }
}

$query->clear();
$query->select('*');
$query->from($database->quoteName('#__mt_cats'));
$query->where('cat_id IN ( '  . implode(',', $catIds) . ')');
$query->where('cat_approved = 1');
$query->where('cat_published = 1');
$database->setQuery($query);
$cats = $database->loadObjectList('cat_id');

foreach ($cats as $cat) {
    $cat->link = JRoute::_( 'index.php?option=com_mtree&task=listcats&cat_id=' . $cat->cat_id);
}

require(JModuleHelper::getLayoutPath('mod_mt_listings_by_category'));