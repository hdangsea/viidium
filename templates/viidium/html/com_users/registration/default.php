<?php
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');

$session = JFactory::getSession();
if (is_null($session->get('code')) || $session->get('code') == "") {
    $session->set('code', JFactory::getApplication()->input->get("code", ""));
}
?>
<div class="registration<?php echo $this->pageclass_sfx ?>">
    <?php if ($this->params->get('show_page_heading')) : ?>
        <div class="page-header">
            <h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
        </div>
    <?php endif; ?>

    <form id="member-registration"
          action="<?php echo JRoute::_('index.php?option=com_users&task=registration.register'); ?>"
          method="post" class="form-validate form-horizontal well uk-form uk-form-horizontal"
          enctype="multipart/form-data">
        <div class="uk-grid" data-uk-grid-margin data-uk-grid-match>
            <?php foreach ($this->form->getFieldsets() as $group => $fieldset): // Iterate through the form fieldsets and display each one.?>
                <?php $fields = $this->form->getFieldset($fieldset->name); ?>
                <?php if (count($fields)): ?>
                    <fieldset class="uk-width-1-<?php echo ($group == 'profile') ? 1 : 2 ?>">
                        <?php if (isset($fieldset->label)):// If the fieldset has a label set, display it as the legend.?>
                            <legend><?php echo JText::_($fieldset->label); ?></legend>
                        <?php endif; ?>

                        <?php if ($group == 'profile') : ?>
                        <div class="tm-user-profile-fieldset">
                            <?php endif; ?>

                            <?php foreach ($fields as $field) :// Iterate through the fields in the set and display them.?>
                                <?php if ($field->hidden):// If the field is hidden, just display the input.?>
                                    <?php echo $field->input; ?>
                                <?php else: ?>
                                    <div class="uk-form-row">
                                        <?php if ($field->type != 'Spacer') : ?>
                                            <div class="uk-form-label">
                                                <?php echo $field->label; ?>
                                                <?php if (!$field->required && $field->type != 'Spacer') : ?>
                                                    <span
                                                        class="optional"><?php echo JText::_('COM_USERS_OPTIONAL'); ?></span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="uk-form-controls">
                                                <?php echo $field->input; ?>
                                            </div>
                                        <?php else : ?>
                                            <?php echo $field->label ?>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                    </fieldset>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <div class="uk-form-row uk-margin-top">
            <div class="uk-form-controls">
                <div class="controls">
                    <button type="submit" class="uk-button uk-button-primary validate">
                        <?php echo JText::_('JREGISTER'); ?>
                    </button>
                    <a class="uk-button uk-button-link" href="<?php echo JRoute::_(''); ?>"
                       title="<?php echo JText::_('JCANCEL'); ?>">
                        <?php echo JText::_('JCANCEL'); ?>
                    </a>
                    <input type="hidden" name="option" value="com_users"/>
                    <input type="hidden" name="task" value="registration.register"/>
                    <input type="hidden" name="code" value="<?php echo $session->get("code") ?>"/>
                </div>
            </div>
        </div>
        <?php echo JHtml::_('form.token'); ?>
    </form>
</div>
