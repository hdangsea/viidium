<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
?>
<div class="tm-reset <?php echo $this->pageclass_sfx ?>">
    <?php if ($this->params->get('show_page_heading')) : ?>
        <h1>
            <?php echo JText::_('TPL_VIIDIUM_RESET_PASSWORD'); ?>
        </h1>
    <?php endif; ?>

    <form id="user-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request'); ?>"
          method="post"
          class="form-validate uk-panel uk-panel-box uk-form uk-form-horizontal">
        <?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
            <fieldset>
                <p><?php echo JText::_($fieldset->label); ?></p>
                <?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
                    <div class="uk-form-row">
                        <div class="uk-form-label">
                            <?php echo $field->label; ?>
                        </div>
                        <div class="uk-form-controls">
                            <?php echo $field->input; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </fieldset>
        <?php endforeach; ?>

        <div class="uk-form-row">
            <div class="uk-form-controls">
                <button type="submit"
                        class="uk-button uk-button-primary validate"><?php echo JText::_('JSUBMIT'); ?></button>
            </div>
        </div>
        <?php echo JHtml::_('form.token'); ?>
    </form>
</div>
