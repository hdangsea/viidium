<?php
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
?>
<div class="tm-login <?php echo $this->pageclass_sfx?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<h1>
			<?php echo $this->escape($this->params->get('page_heading')); ?>
		</h1>
	<?php endif; ?>

	<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
	<div class="login-description">
	<?php endif; ?>

		<?php if ($this->params->get('logindescription_show') == 1) : ?>
			<?php echo $this->params->get('login_description'); ?>
		<?php endif; ?>

		<?php if (($this->params->get('login_image') != '')) :?>
			<img src="<?php echo $this->escape($this->params->get('login_image')); ?>" class="login-image" alt="<?php echo JTEXT::_('COM_USER_LOGIN_IMAGE_ALT')?>"/>
		<?php endif; ?>

	<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
	</div>
	<?php endif; ?>

	<form action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post"
        class="form-validate uk-panel uk-panel-box uk-form uk-form-horizontal">
		<fieldset>
			<?php foreach ($this->form->getFieldset('credentials') as $field) : ?>
				<?php if (!$field->hidden) : ?>
					<div class="uk-form-row">
						<div class="uk-form-label">
							<?php echo $field->label; ?>
						</div>
						<div class="uk-form-controls">
							<?php echo $field->input; ?>
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>

			<?php if ($this->tfa): ?>
				<div class="uk-form-row">
					<div class="uk-form-label">
						<?php echo $this->form->getField('secretkey')->label; ?>
					</div>
					<div class="uk-form-controls">
						<?php echo $this->form->getField('secretkey')->input; ?>
					</div>
				</div>
			<?php endif; ?>

			<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
                <div class="uk-form-row">
                    <div class="uk-form-controls">
                        <input id="remember" type="checkbox" name="remember" class="inputbox" value="yes"/>
                        <label><?php echo JText::_('COM_USERS_LOGIN_REMEMBER_ME') ?></label>
                    </div>
                </div>
			<?php endif; ?>

			<div class="uk-form-row">
				<div class="uk-form-controls">
					<button type="submit" class="uk-button uk-button-primary">
						<?php echo JText::_('JLOGIN'); ?>
					</button>
				</div>
			</div>

            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
                        <?php echo JText::_('COM_USERS_LOGIN_RESET'); ?>
                    </a>
                    &nbsp;|&nbsp;
                    <a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
                        <?php echo JText::_('COM_USERS_LOGIN_REMIND'); ?>
                    </a>
                </div>
            </div>
            <?php echo JHtml::_('form.token'); ?>
            <input type="hidden" name="return" value="<?php echo base64_encode($this->params->get('login_redirect_url', $this->form->getValue('return'))); ?>" />
		</fieldset>
	</form>
</div>
