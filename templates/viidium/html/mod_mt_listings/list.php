<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php
    $app = JFactory::getApplication();
    $menu = $app->getMenu();
    include_once JPATH_BASE . '/components/com_viidium/libs/Parsedown.php';

    jimport('joomla.access.access');
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select('*');
    $query->from($db->quoteName('#__usergroups'));
    $db->setQuery($query);
    $allGroups = $db->loadObjectList('id');

?>
<?php if ($moduleHelper->isListingPage() || ($menu->getActive() == $menu->getDefault())) : ?>
    <ul id="<?php echo $uniqid; ?>" class="mod_mt_listings vi-mt-listings-list uk-list">
        <?php
            global $mtconf;
        ?>
        <?php if ( is_array($listings) ) : ?>
            <?php foreach ($listings as $idx => $l) : ?>
                <?php
                    $user = JFactory::getUser($l->user_id);
                    $arrFields = $fields[$l->link_id];
                    $desField = $arrFields->getFieldById(2);
                    $maxChars = $desField->getParam('summaryChars');
                    $stripSummaryTags = $desField->getParam('stripSummaryTags', 1);
                ?>
                <?php if (!$l->link_featured) : ?>
                    <li class="">
                        <div class="uk-grid uk-position-relative">
                            <div class="uk-width-2-5">
                                <?php
                                if(isset($l->image_path) && !empty($l->image_path)) {
                                    $img = $l->image_path;
                                } else {
                                    $img = $mtconf->getjconf('live_site') . $mtconf->get('relative_path_to_images') . 'noimage_thb.png';
                                }
                                ?>
                                <div class="vi-listing-image uk-animation-scale" style="background-image: url('<?php echo $img?>')"></div>
                            </div>
                            <div class="uk-width-3-5">
                                <h3>
                                    <a class="vi-listing-title" href="<?php echo $l->link?>">
                                        <?php echo $l->link_name?>
                                    </a>
                                </h3>

                                <div class="vi-listing-des">
                                    <?php
                                        $parseDown = new Parsedown();
                                        $desc = $parseDown->text($l->link_desc);
                                        $desc = trim(JString::substr($desc, 0, $maxChars));
                                        if ($stripSummaryTags) {
                                            echo strip_tags($desc);
                                        } else {
                                            echo $desc;
                                        }

                                        $userUrl = JRoute::_( 'index.php?option=com_mtree&task=viewowner&user_id=' . $user->id);
                                        $groups = JAccess::getGroupsByUser($user->id, false);
                                    ?>
                                </div>

                                <div class="vi-profile-info">
                                    <div class="uk-display-inline-block uk-text-right vi-info">
                                        <div class="uk-text-muted">
                                            <a href="<?php echo $userUrl?>" title="<?php echo $user->name?>"><?php echo $user->name?></a>
                                        </div>
                                        <div class="uk-text-muted vi-user-group">
                                            <?php foreach ($groups as $gId) : ?>
                                                <?php
                                                    $group = $allGroups[$gId];
                                                    echo '<span>' . $group->title . '</span>';
                                                ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="uk-display-inline-block">
                                        <?php
                                            // Show Owner Profile Picture
                                            $profilepictureEnabled = JPluginHelper::isEnabled('user', 'profilepicture');
                                        ?>
                                        <?php if ($profilepictureEnabled) : ?>
                                            <a href="<?php echo $userUrl?>" title="<?php echo $user->name?>">
                                                <?php
                                                    jimport('mosets.profilepicture.profilepicture');
                                                    $profilepicture = new ProfilePicture($l->user_id);
                                                    if ($profilepicture->exists()) {
                                                        echo '<img src="' . $profilepicture->getURL(PROFILEPICTURE_SIZE_200) . '" alt="' . $user->name . '" />';
                                                    } else {
                                                        echo '<img src="' . $profilepicture->getFillerURL(PROFILEPICTURE_SIZE_200) . '" alt="' . $user->name . '" />';
                                                    }
                                                ?>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
            <?php if ($show_more) : ?>
                <li class="showmore">
                    <a href="<?php echo $show_more_link?>" class="<?php echo $listingclass?>"><?php echo $caption_showmore?></a>
                </li>
            <?php endif; ?>
        <?php endif; ?>
    </ul>
<?php endif; ?>