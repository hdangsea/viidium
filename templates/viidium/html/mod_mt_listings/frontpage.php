<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php
    $warp = require(__DIR__.'/../../warp.php');
    $warp['asset']->addFile('js', 'media/mod_mt_listings/script.js');

    foreach ($listings as $l) {
        $l->original_image_path =
            $mtconf->getjconf('live_site') . $mtconf->get('relative_path_to_listing_original_image') . $l->image;
    }
    $height = 445;
?>
<style type="text/css">
    .tm-image-post {
        height: <?php echo $height?>px !important;
    }
</style>
<div  class="uk-slidenav-position vi-slideshow">
    <ul class="mod_mt_listings mod_mt_listings_frontpage uk-slideshow" data-uk-slideshow="{autoplay: false, height: '<?php echo $height?>px'}">
        <?php
            global $mtconf;
        ?>
        <?php if ( is_array($listings) ) : ?>
            <?php foreach ($listings as $l) : ?>
                <?php
                $user = JFactory::getUser($l->user_id);
                ?>
                <li class="uk-panel">
                    <img class="tm-image-post" src="<?php echo $l->original_image_path?>" />
                    <div class="tm-fp-featured-post">
                        <h2 class="vi-post-title"><a href="<?php echo $l->link?>"><?php echo $l->link_name?></a></h2>
                    </div>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>
    <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
    <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
</div>