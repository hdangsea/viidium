jQuery(function($) {
    $(document).ready(function() {
        var initScroll;

        var scrollFn = function() {
            var scrollTop = $(document).scrollTop();

            // fix top header and sidebar
            fixEles.each(function (idx, ele) {
                var e = $(ele);
                var data = e.data('vi-fixed');
                var target;
                if (typeof data == 'object') {
                    target = e.find(data.target);
                } else {
                    target = e;
                }
                var top = target.position().top - (scrollTop - initScroll);
                if (top <= diffEleHeights[idx] || top >= eleTops[idx]) {
                    if (top > eleTops[idx]) {
                        top = eleTops[idx];
                    } else {
                        top = diffEleHeights[idx];
                    }
                }
                target.css('top', top + 'px');

                initScroll = scrollTop;
            });
        };

        var fixEles = $('[data-vi-fixed]');
        var windowHeight = $(window).height();

        $('[data-fixed-parent]').each(function(idx, ele) {
            if ($(ele).data('position') !== 'top') {
                var parent = $(this).parent();
                $(this).width(parent.width());
            }
        });

        $.UIkit.$doc.on('show.uk.offcanvas', function(e, element, bar) {
            // fix video position
            var topHtml = $('html').position().top;
            $('video[data-position-fixed=1]').css('top', topHtml + 'px');

            var barOuterWidth = $(bar).outerWidth();
            $.UIkit.$doc.data('bar-width', barOuterWidth);

            fixEles.each(function (idx, ele) {
                var e = $(ele);
                var data = e.data('vi-fixed');
                var target;
                if (typeof data == 'object') {
                    target = e.find(data.target);
                } else {
                    target = e;
                }

                target.animate({
                    left: '+=' + barOuterWidth,
                    top: '+=0'
                }, 200);
            });

            var left = $('[data-fixed-parent]').offset().left;
            $('[data-fixed-parent]').animate({
                left: left + barOuterWidth
            }, 200);

            $('[data-fixed-parent]').data('left', left);

            $(element).appendTo($('body'));
        });

        $.UIkit.$doc.on('hide.uk.offcanvas', function(element, bar) {
            // fix video position
            $('video[data-position-fixed=1]').css('top', 0);

            var barOuterWidth = $.UIkit.$doc.data('bar-width');

            fixEles.each(function (idx, ele) {
                var e = $(ele);
                var data = e.data('vi-fixed');
                var target;
                if (typeof data == 'object') {
                    target = e.find(data.target);
                } else {
                    target = e;
                }

                target.animate({
                    left: '-=' + barOuterWidth,
                    top: '-=0'
                }, 100);
            });

            var left = $('[data-fixed-parent]').data('left');
            $('[data-fixed-parent]').animate({
                left: left
            }, 100);
        });

        if (fixEles.length > 0) {
            var diffEleHeights = [];
            var eleTops = [];

            $(window).load(function () {
                $('body').addClass('loaded');

                fixEles.each(function (idx, ele) {
                    var e = $(ele);
                    var data = e.data('vi-fixed');
                    var target;
                    var offset = 0;
                    if (typeof data == 'object') {
                        target = e.find(data.target);
                        offset = data.offset;
                    } else {
                        target = e;
                    }
                    var top = target.offset().top;
                    var left = target.offset().left;
                    var height = target.height();

                    target.width(target.width());
                    target.height(height);
                    target.css('top', top + 'px');
                    target.css('left', left + 'px');
                    target.css('position', 'fixed');

                    diffEleHeights[idx] = top - height + windowHeight - offset;
                    eleTops[idx] = top;
                });

                initScroll = $(document).scrollTop();
                $(window).bind('scroll', scrollFn);
            });
        }

        $('.vi-wrap-left-bar video').height(windowHeight);

        if ($('.vi-footer-bar').length > 0) {
            var height = $('.vi-footer-bar').height();
            $('.vi-footer-bar').parent().css('margin-bottom', height + 'px');
        }
    });
});
