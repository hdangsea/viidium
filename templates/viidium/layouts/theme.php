<?php
// get theme configuration
include($this['path']->path('layouts:theme.config.php'));

?>
<!DOCTYPE HTML>
<html lang="<?php echo $this['config']->get('language'); ?>" dir="<?php echo $this['config']->get('direction'); ?>"  data-config='<?php echo $this['config']->get('body_config','{}'); ?>'>

<head><?php echo $this['template']->render('head'); ?></head>

<body class="<?php echo $this['config']->get('body_classes'); ?>">
    <?php if ($this['widgets']->count('content-top')) : ?>
        <section class="<?php echo $display_classes['content-top']; ?>"
                 data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
            <?php echo $this['widgets']->render('content-top', array('layout'=>$this['config']->get('grid.content-top.layout'))); ?>
        </section>
    <?php endif; ?>

    <div class="vi-wrap-left-bar">
        <div class="vi-left-bar">
            <div id="tm-canvas-bar" class="uk-offcanvas">
                <?php if ($this['widgets']->count('menu + copyright')) : ?>
                    <div class="uk-offcanvas-bar">
                        <?php echo $this['widgets']->render('menu'); ?>

                        <?php if ($this['widgets']->count('copyright')) : ?>
                            <div class="vi-copyright">
                                <?php echo $this['widgets']->render('copyright');?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>

            <a id="tm-menu-button" data-uk-offcanvas href="#tm-canvas-bar">
                <span><?php echo JText::_('TPL_VIIDIUM_OPEN_MENU')?></span>
            </a>
            <?php if ($this['widgets']->count('logo')) : ?>
                <a class="tm-logo" href="<?php echo $this['config']->get('site_url'); ?>"><?php echo $this['widgets']->render('logo'); ?></a>
            <?php endif; ?>
        </div>
        <video autoplay loop id="bgvid" data-position-fixed="1">
            <source src="images/main.mp4" poster="images/main.jpg" type="video/mp4">
        </video>
    </div>

    <div class="tm-right-bar">
        <?php if ($this['config']->get('headerbar-pos') == 'top'
            && $this['widgets']->count('tagline + topbar + userbox + search + headerbar')) : ?>
            <?php echo $this->render('theme.headerbar');?>
        <?php endif; ?>

        <div class="uk-grid">
            <div class="<?php echo $columns['main']['class']?>">
                <?php if ($this['widgets']->count('toolbar-l + toolbar-r')) : ?>
                    <div class="tm-toolbar uk-clearfix uk-hidden-small">

                        <?php if ($this['widgets']->count('toolbar-l')) : ?>
                            <div class="uk-float-left"><?php echo $this['widgets']->render('toolbar-l'); ?></div>
                        <?php endif; ?>

                        <?php if ($this['widgets']->count('toolbar-r')) : ?>
                            <div class="uk-float-right"><?php echo $this['widgets']->render('toolbar-r'); ?></div>
                        <?php endif; ?>

                    </div>
                <?php endif; ?>

                <?php if ($this['widgets']->count('top-a + top-b + main-top + main-bottom + bottom-a + bottom-b')
                    || $this['config']->get('system_output', true)) : ?>
                    <?php if ($this['widgets']->count('top-a')) : ?>
                        <section class="<?php echo $grid_classes['top-a']; echo $display_classes['top-a']; ?>"
                                 data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
                            <?php echo $this['widgets']->render('top-a', array('layout'=>$this['config']->get('grid.top-a.layout'))); ?>
                        </section>
                    <?php endif; ?>

                    <?php if ($this['widgets']->count('top-b')) : ?>
                        <section class="<?php echo $grid_classes['top-b']; echo $display_classes['top-b']; ?>"
                                 data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
                            <?php echo $this['widgets']->render('top-b', array('layout'=>$this['config']->get('grid.top-b.layout'))); ?>
                        </section>
                    <?php endif; ?>

                    <?php if ($this['widgets']->count('main-top')) : ?>
                        <section class="<?php echo $grid_classes['main-top']; echo $display_classes['main-top']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('main-top', array('layout'=>$this['config']->get('grid.main-top.layout'))); ?></section>
                    <?php endif; ?>

                    <?php if ($this['config']->get('system_output', true)) : ?>
                        <main class="tm-content">
                            <?php if ($this['widgets']->count('breadcrumbs')) : ?>
                                <?php echo $this['widgets']->render('breadcrumbs'); ?>
                            <?php endif; ?>

                            <?php echo $this['template']->render('content'); ?>
                        </main>
                    <?php endif; ?>

                    <?php if ($this['widgets']->count('main-bottom')) : ?>
                        <section class="<?php echo $grid_classes['main-bottom']; echo $display_classes['main-bottom']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
                            <?php echo $this['widgets']->render('main-bottom', array('layout'=>$this['config']->get('grid.main-bottom.layout'))); ?>
                        </section>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if ($this['widgets']->count('bottom-a')) : ?>
                    <section class="<?php echo $grid_classes['bottom-a']; echo $display_classes['bottom-a']; ?>"
                             data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
                        <?php echo $this['widgets']->render('bottom-a', array('layout'=>$this['config']->get('grid.bottom-a.layout'))); ?>
                    </section>
                <?php endif; ?>

                <?php if ($this['widgets']->count('bottom-b')) : ?>
                    <section class="<?php echo $grid_classes['bottom-b']; echo $display_classes['bottom-b']; ?>"
                             data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
                        <?php echo $this['widgets']->render('bottom-b', array('layout'=>$this['config']->get('grid.bottom-b.layout'))); ?>
                    </section>
                <?php endif; ?>
            </div>

            <?php
                $sidebarsConfig = $this['config']->get("sidebars");
            ?>
            <?php foreach($columns as $name => &$column) : ?>
                <?php if ($name != 'main' && $this['widgets']->count("{$name}1 + {$name}2 + {$name}3")) : ?>
                    <aside class="<?php echo $column['class'] ?>"
                        <?php echo (isset($sidebarsConfig[$name]['fixed']) && $sidebarsConfig[$name]['fixed'])?'data-vi-fixed={"target":".vi-sidebar","offset":"80"}':''?>>
                        <?php
                            if ($this['config']->get('headerbar-pos') == $name && $this['widgets']->count('headerbar + userbox + search')) {
                                echo $this->render('theme.headerbar');
                            }
                        ?>
                        <div class="vi-sidebar <?php echo ($this['widgets']->count('headerbar + userbox'))?'vi-sidebar-padding-top':''?>">
                            <?php if ($this['widgets']->count($name . '1')) : ?>
                                <div class="vi-sidebar-<?php echo $name?>1 <?php echo $grid_classes["{$name}1"]; echo $display_classes["{$name}1"]; ?>">
                                    <?php echo $this['widgets']->render($name . '1', array('layout'=>$this['config']->get("grid.{$name}1.layout")));?>
                                </div>
                            <?php endif; ?>
                            <?php if ($this['widgets']->count($name . '2')) : ?>
                                <div data-uk-grid-margin class="vi-sidebar-<?php echo $name?>2 <?php echo $grid_classes["{$name}2"]; echo $display_classes["{$name}2"]; ?>">
                                    <?php echo $this['widgets']->render($name . '2', array('layout'=>$this['config']->get("grid.{$name}2.layout")));?>
                                </div>
                            <?php endif; ?>
                            <?php if ($this['widgets']->count($name . '3')) : ?>
                                <div class="vi-sidebar-<?php echo $name?>3 <?php echo $grid_classes["{$name}3"]; echo $display_classes["{$name}3"]; ?>">
                                    <?php echo $this['widgets']->render($name . '3', array('layout'=>$this['config']->get("grid.{$name}3.layout")));?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </aside>
                <?php endif ?>
            <?php endforeach ?>
        </div>

        <?php if ($this['widgets']->count('content-bottom')) : ?>
            <section class="<?php echo $grid_classes['content-bottom']; echo $display_classes['content-bottom']; ?>"
                     data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
                <?php echo $this['widgets']->render('content-bottom', array('layout'=>$this['config']->get('grid.content-bottom.layout'))); ?>
            </section>
        <?php endif; ?>
    </div>

    <?php if ($this['widgets']->count('footer + debug')) : ?>
        <footer class="tm-footer">
            <?php
                echo $this['widgets']->render('footer');
                echo $this['widgets']->render('debug');
            ?>
        </footer>
    <?php endif; ?>

	<?php if ($this['widgets']->count('offcanvas')) : ?>
        <div id="offcanvas" class="uk-offcanvas">
            <div class="uk-offcanvas-bar"><?php echo $this['widgets']->render('offcanvas'); ?></div>
        </div>
	<?php endif; ?>
</body>

</html>