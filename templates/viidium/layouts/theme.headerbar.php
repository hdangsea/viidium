<div class="tm-headerbar">
    <div data-fixed-parent="1" class="vi-headerbar-top" data-position="<?php echo $this['config']->get('headerbar-pos')?>">
        <?php if ($this['widgets']->count('tagline')) : ?>
            <div class="tm-tagline">
                <?php echo $this['widgets']->render('tagline', array('layout'=>$this['config']->get('grid.tagline.layout'))); ?>
            </div>
        <?php endif; ?>

        <div class="vi-userbox-search">
            <?php if ($this['widgets']->count('userbox')) : ?>
                <div class="uk-panel vi-userbox">
                    <a href="#" data-uk-toggle="{target:'.vi-userbox-panel'}"><i class="uk-icon-user"></i></a>
                </div>
            <?php endif; ?>

            <div class="vi-search">
                <?php if ($this['widgets']->count('search')) : ?>
                    <?php echo $this['widgets']->render('search');?>
                <?php endif;?>
            </div>
        </div>

        <div class="vi-topbar-content">
            <?php echo $this['widgets']->render('topbar') ?>
        </div>
    </div>

    <div class="vi-headerbar-content">
        <?php echo $this['widgets']->render('headerbar') ?>
        <div class="vi-headerbar-headline">
            <?php echo $this['widgets']->render('headerbar-headline') ?>
        </div>
    </div>

    <?php if ($this['widgets']->count('userbox')) : ?>
        <div class="vi-userbox-panel uk-hidden">
            <div class="uk-animation-fade tm-block-userbox<?php //echo $block_classes['userbox'];?>">
                <?php echo $this['widgets']->render('userbox') ?>
            </div>
        </div>
    <?php endif; ?>
</div>

<div class="uk-clearfix"></div>