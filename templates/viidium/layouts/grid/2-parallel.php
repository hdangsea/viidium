<?php
foreach ($widgets as $index => $widget) {
    $classes = array();
    $prev = 0;
    foreach ($displays as $class => &$display) {
        if (false !== $pos = array_search($index, $display)) {
            $width = in_array($class, $stacked) ? '1-1' : '1-2';
            if ($width != $prev) {
                $classes[] = "uk-width".($class != 'small' ? "-{$class}" : '')."-{$width}";
                $prev = $width;
            }
        } else {
            $classes[] = "uk-hidden-{$class}";
        }
    }

    printf(PHP_EOL.'<div class="%s">%s</div>'.PHP_EOL, implode(' ', $classes), $widget);
}