<?php

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

class plgUserViidium extends JPlugin
{
    function onUserBeforeSave($user, $isNew)
    {
        $app =& JFactory::getApplication();
        if ($isNew && $app->isSite()) {
            $input = JFactory::getApplication()->input;

            $code = $input->get("code", "");
            if (!$this->validate_code($code)) {
                return false;
            }

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $fields = array(
                $db->quoteName('used') . ' = 1'
            );
            $conditions = array(
                $db->quoteName('code') . ' = "' . $code . '"'
            );
            $query->update($db->quoteName('#__vd_invitations'))->set($fields)->where($conditions);
            $db->setQuery($query);
            $db->execute();
            $session = JFactory::getSession();
            $session->set('code', '');
        }
    }

    function validate_code($code)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(array('used')));
        $query->from($db->quoteName('#__vd_invitations'));
        $query->where($db->quoteName('code') . ' = "' . $code . '"', $db->quoteName('used') . ' = 0');
        $db->setQuery($query);
        $db->execute();
        $used = $db->loadObjectList();
        if (!isset($used[0]) || $used[0]->used == 1) {
            $mainframe = JFactory::getApplication();
            $mainframe->enqueueMessage("Invalid Code", 'warning');
            $session = JFactory::getSession();
            $session->set('code', '');
            return false;
        }
        return true;
    }

    function onUserAfterSave($user, $isnew, $success, $msg)
    {
        $params = JComponentHelper::getParams('com_viidium');
        if ($isnew) {
            $db = JFactory::getDbo();

            $query = $db->getQuery(true);

            $columns = array('user_id', 'remaining_invitation');

            $values = array($user['id'], $params->get("remaining_invitations", 5));

            $query
                ->insert($db->quoteName('#__vd_users'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $db->execute();
        }
    }


}
